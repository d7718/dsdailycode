/*
   Program 3: WAP that dynamically allocates a 2-D array of integers,takes value from the user, and prints it.[Use malloc]
              rows=3
	      columns=4
*/


#include<stdio.h>
#include<stdlib.h>

void main(){


	int rows =3,columns=4;

	int *ptr = (int*) malloc(rows*columns*sizeof(int));

	printf("Enter the elements:\n");

	for(int i=0;i<rows;i++){

		for(int j=0;j<columns;j++){

                          scanf("%d",(ptr+(i*rows)+j));
	         }
		printf("\n");

	}
	
	for(int i=0;i<rows;i++){
		
		for(int j=0;j<columns;j++){
                          
		      printf("%d ",*(ptr+(i*rows)+j)); 
	         }
		printf("\n");
	}

        free(ptr);



}
