/*
  Program 4: WAP that dynamically allocates a 3-D array of integers,takes value from the user, and prints it.[Use malloc]
                planes=2  rows=3 columns=3
*/

#include<stdio.h>
#include<stdlib.h>
void main(){

        int planes =2,rows =3 ,columns=3;

	int *ptr = (int*) malloc(planes*rows*columns*sizeof(int));

	printf("Enter the elements:\n");

        for(int i =0;i<planes;i++){

       	       for(int j=0;j<rows;j++){

			for(int k=0;k<columns;k++){

        	                  scanf("%d",(ptr+(i*rows*columns)+(j*rows)+k)); 
       			}
			printf("\n");
		}
	}
	

       for(int i =0;i<planes;i++){
	
       	       for(int j=0;j<rows;j++){
		
			for(int k=0;k<columns;k++){
                          
		                printf("%d",*(ptr+(i*rows*columns)+(j*rows)+k)); 
			}
	         
			printf("\n");
       	       }
	      printf("\n");
	}

       free(ptr);
}

