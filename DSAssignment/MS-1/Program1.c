/*
  Program 1 : Write the Output of following Code
 
*/


#include<stdio.h>

int fun(int);

void main(){

	int s;

	s = fun(10);

	printf("%d\n",s);

}

int fun(int s){

	//s>20 ? return(9) : return(20); error: expected expression before ‘return’
	
	return(s>20 ? 9 : 20 );
		   

}

