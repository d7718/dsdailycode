/*
  Program 2: WAP that dynamically allocates a 1-D array of marks,takes value from the user, and prints it.[Use malloc]
*/

#include<stdio.h>
#include<stdlib.h>


void main(){


	int n ;

	printf("Total No of Marks:\n");
	scanf("%d",&n);

	float *marks = (float*) malloc(n* sizeof(float));

	printf("Enter the Marks:\n");

	for(int i=0;i<n;i++){

	         scanf("%f",(marks+i)); 

	}

	printf("Marks\n");

	for(int i=0;i<n;i++){
	   
	    printf("%f\n",*(marks+i));

	 }

	free(marks);

	
}
