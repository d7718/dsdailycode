/*
   LinkedList Structure:

        struct Node{

	         int data;          // Data element
		 struct Node *next; // Address of next node
	 };


  Program 1 : 
               write a program that searches for first occurence of a particular element from a singly linear linked list.
	       
               Input:|10|-->|20|-->|30|-->|40|-->|50|-->|30|
	       Input:
	              Enter Number : 30

	       Output: 3

  Program 2 :
               write a program that searches for second last occurence of a particular element from a singly linear linked list.
	      
               Input:|10|-->|20|-->|30|-->|40|-->|30|-->|30|
               Input:
                      Enter Number : 30

               Output: 5

  Program 3 : 
               write a program that searches for occurence of a particular element from a singly linear linked list.

	       Input:|10|-->|20|-->|30|-->|40|-->|50|-->|30|
               Input:
                      Enter Number : 30

               Output: 2 times

  Program 4 :
               write a program that adds the digits of a data element from singly linear linked list and changes the data.

              Input:|11|-->|12|-->|13|-->|141|-->|2|-->|158|

              Output:
	             |2|-->|3|-->|4|-->|6|-->|2|-->|14|

  Program 5 :
               write a program that searches all palindrome data elements from singly linear linked list.And print position of palindrome data.
              Input:|12|-->|121|-->|30|-->|252|-->|35|-->|151|

              Output:
	              Palindrome found at 2
		      Palindrome found at 4
		      Palindrome found at 6

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}Node;


Node *head = NULL;

//createNode

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));

	printf("Enter Data:\n");
	scanf("%d",&(newNode->data));

	newNode->next = NULL;

	return newNode;
}


int countNode(){

	int count = 0;

	Node *temp = head;

	while(temp != NULL){

		count++;
		temp = temp->next;
	}

	return count;
}


//addNode

void addNode(){

       Node *newNode = createNode();

       if(head==NULL){

             head = newNode;

       }else{

              Node *temp = head;

	      while(temp->next != NULL){

		      temp = temp->next;

	       }

	        temp->next = newNode;
	}
}



//printLL

void printLL(){

	if(head==NULL){

		printf("LINKEDLIST IS EMPTY !\n");
	}else{
		Node *temp = head;

		while(temp->next != NULL){

			printf("|%d|->",temp->data);
			temp = temp->next;
		}

		printf("|%d|\n",temp->data);
	}
}


//Program 1 : FirstOccurence

void firstOccurence(int num){

	int index = 0,flag=0;
	
	Node *temp = head;

	while(temp!=NULL){

		index++;

		if(temp->data == num){

			flag=1;
			break;
		}

		temp = temp->next;
	}

	if(flag==0){

		printf("%d is Not Present in LinkedList !\n",num);
	}else{
		printf("%d is Present in LinkedList at Index %d\n",num,index);
	}
}


//Program 2 : SecondLastOccurence

void secondLastOccurence(int num){

	int index = 0,occ1=0,occ2=0;

	Node *temp = head;

	while(temp != NULL){

	       index++;

	       if(temp->data == num){

		       occ2 = occ1;
		       occ1 = index;
		}

	        temp = temp->next;
	}

	if(occ1==0){

		printf("%d is Not Present in LinkedList!\n",num);
	}else{

		if(occ2){

			printf("%d is Present and it's Second Last Occurence is at Index %d\n",num,occ2);
		}else{
			printf("%d is Present Only once at Index %d\n",num,occ1);
		}
	}
}


//Program 3 :OccurenceCount

void occurenceCount(int num){

	int count = 0;

	Node *temp = head;

	while(temp != NULL){

		if(temp->data == num){

			count++;
		}
	        
		temp = temp->next;
	}

	printf("%d is present %d times in LinkedList\n",num,count);
}


//Program 4 : SumOfDataElement

int sumOfDigit(int num){

       int sum=0;

       while(num!=0){

	       sum+=num%10;
	       num/=10;
       }

       return sum;
}


void sumOfData(){

	Node *temp = head;

	while(temp != NULL){

		temp->data = sumOfDigit(temp->data);
		temp = temp->next;
	}

	printLL();

}

	   
//Program 5 : CheckPalindrome

//Way-1

int palindrome(int num){

	int rev=0, temp=num;

	while(temp != 0){

		rev = rev*10 + temp%10;
		temp/=10;
	}

	if(num==rev)

		return 1;
	else
		return 0;
}


void checkPalindrome(){

	int index=0,flag=0;

	Node *temp = head;

	while(temp != NULL){

		index++;

		if(palindrome(temp->data)){

			printf("%d is Palindrome at index %d\n",temp->data,index);
			flag=1;
		}
	
		temp = temp->next;
	}

	if(flag==0)
		printf("No Palindrome Present!\n");
}


//Way-2

void checkPalindrome1(){

	Node *temp =head;

	int flag=1;
	int i=0;

	while(temp !=NULL){

		i++;

		int temp1,sum=0,rem;
		temp1 = temp->data;

		while(temp1 != 0){

			rem = temp1%10;
			sum = (sum*10)+rem;
			temp1/=10;
		}

		if(temp->data==sum){

			printf("Palidrome at %d\n",i);
			flag=0;
		}
   
		temp=temp->next;
	}

	if(flag==1){

		printf("No Palindrome Found!\n");
	}
}


void main(){

	int n;
	char ch;

	printf("Enter No of Nodes:\n");
	scanf("%d",&n);
        

	if(n>0){
	
		for(int i =0;i<n;i++){

			addNode();
		}

		printLL();

		do{

			printf("*----Main Menu----*\n");
			printf("1.FirstOccurence\n");
			printf("2.SecondLastOccurence\n");
			printf("3.OccurenceCount\n");
			printf("4.SumOfDataElement\n");
			printf("5.CheckPalindrome\n");
			printf("6.AddNode\n");
			printf("7.PrintLL\n");

			int choice;

			printf("Enter Your Choice:\n");
			scanf("%d",&choice);

			switch(choice){

				case 1: 
					{
			          		int num;
				  
				  		printf("Enter number to be Searched:\n");
				  		scanf("%d",&num);

				  		firstOccurence(num);
			
					}
						  break;

		       		case 2:
				        {
				      		 if(countNode()>=2){

					       		int num;

					       		printf("Enter number to be Searched:\n");
					       		scanf("%d",&num);

					       		secondLastOccurence(num);
						
						 }else{
							printf("Minimum 2 Nodes required !\n");
						}

				    	}

				     			break;

				case 3:
					 {
			               		int num;

                                       		printf("Enter number to be Searched:\n");
                                       		scanf("%d",&num);

                                       		occurenceCount(num);

				  	 }
				    		 break;


		       		case 4 : 
				    		 sumOfData();
				    		 break;

		       		case 5 :
						 checkPalindrome();
				   		 break;

		       		case 6 :
				    		 addNode();
				    		 break;

		       		case 7 :
				     		printLL();
				     		break;

		      		default:
				     		printf("Invalid Choice !\n");
				     		break;

			}

			getchar();

			printf("Do You Want to Continue?:\n");
			scanf("%c",&ch);

		}while(ch=='y' || ch == 'Y');


	}else{
		printf("Invalid Node Count!\n");

	}


}









