/*
   LinkedList Structure:

        struct Node{

	         char data[20];          // Data element
		 struct Node *next; // Address of next node
	 };


  Program 6 :
             Write a program that accepts a singly linear linked list from user.Take number from user and print data of the length of that 
	     number.

             Input:|Shashi|-->|Ashish|-->|Kanha|-->|Rahul|-->|Badhe|
	     Input:
	            Enter Number:5

	     Output:
	           Kanha
		   Rahul
		   Badhe

  Program 7 :
            Write a program that accepts a singly linear linked list from user.Reverse the data elements from linkedlist.

	     Input:|Shashi|-->|Ashish|-->|Kanha|-->|Rahul|-->|Badhe|

             Output:|ihsahS|-->|hsihsA|-->|ahnaK|-->|luhaR|-->|ehdaB|

 Program 8 :
             Write a program that accepts a singly linear linked list from user.Take number from user and only keep elements that are equalin              length to that number and delete other elements. And print the LinkedList

	     Input:|Shashi|-->|Ashish|-->|Kanha|-->|Rahul|-->|Badhe|
             Input:
                    Enter Number:6

	     Output:|Shashi|-->|Ashish|

*/
               

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	char data[20];
	struct Node *next;
}Node;


Node *head = NULL;

//createNode

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));

	printf("Enter Data:\n");

	int i =0;
	char ch;

	while((ch=getchar())!='\n'){

		(*newNode).data[i++] = ch;
	}

	newNode->next = NULL;

	return newNode;
}

// Mystrlen

int mystrlen(char *str){

	int len = 0;

	while(*str != '\0'){

		len++;
		str++;
	}

	return len;
}

int countNode(){

	int count = 0;

	Node *temp = head;

	while(temp != NULL){

		count++;
		temp = temp->next;
	}

	return count;
}


//addNode

void addNode(){

       Node *newNode = createNode();

       if(head==NULL){

             head = newNode;

       }else{

              Node *temp = head;

	      while(temp->next != NULL){

		      temp = temp->next;

	       }

	        temp->next = newNode;
	}
}




//printLL

void printLL(){

	if(head==NULL){

		printf("LINKEDLIST IS EMPTY !\n");
	}else{
		Node *temp = head;

		while(temp->next != NULL){

			printf("|%s|->",temp->data);
			temp = temp->next;
		}

		printf("|%s|\n",temp->data);
	}
}


// Program 6 : StringLength

void stringLength(int len){

	Node *temp = head;
	int flag=0;

	while(temp!=NULL){

		int len = mystrlen(temp->data);

		if(mystrlen(temp->data)==len){

			printf("|%s|",temp->data);
			flag=1;
		}

		temp = temp->next;
	}

	if(flag==0)
		printf("There is no data with given length!\n");
}


//Program 7 : StringReverse

void strrev(char str[]){

     int len = mystrlen(str);
     char temp;

     for(int i=0;i<len/2;i++){

                    temp = str[i];
                    str[i] = str[len-1-i];
                    str[len-1-i] = temp;
     }

     
}


void stringReverse(){

	Node *temp = head;

	while(temp != NULL){

		strrev(temp->data);
        
		temp = temp->next;
	}
}




//Program 8: DeleteData
 
//deleteFirst

void deleteFirst(){


	if(head==NULL){

		printf("LinkedList is EMPTY !\n");
	
	}else{

		Node *temp = head;
		head = temp->next;
		free(temp);
	}
}


//deleteLast

void deleteLast(){


	if(head==NULL){
		
		printf("LinkedList is EMPTY !\n");
	
	}else{ 
		
	     Node *temp = head;
	     	
	     	if(head->next==NULL){

			     free(temp);
			     head=NULL;
		
		}else{

	                while(temp->next->next !=NULL){

		                  temp = temp->next;
                	}

			free(temp->next);
			temp->next = NULL;

	     }
       }
}


// deleteAtPos

void deleteAtPos(int pos){


	int cnt = countNode();

	if(pos<=0 || pos>cnt){

		printf("Invalid Position!\n");
	
	}else{
		if(pos==1)

			deleteFirst();

		else if(pos==cnt)

			deleteLast();

		else{
			Node *temp1 = head;


			while(pos-2){

				temp1 = temp1->next;
				pos--;
			}

			Node *temp2 = temp1->next;
			temp1->next = temp2->next;
			free(temp2);
		}
	}
}


void deleteData(int len){

	Node *temp = head;
	int index=0i,flag=0;

	while(temp != NULL){

		index++;

		if(mystrlen(temp->data)!=len){

			deleteAtPos(index);
			flag=1;
			index--;
		}
	
		temp = temp->next;
	}

	if(flag==0)

		printf("All data length is equal to given length!\n");

	printLL();

}

void main(){

	int n;
	char ch;

	printf("Enter No of Nodes:\n");
	scanf("%d",&n);

	getchar();
        

	if(n>0){
	
		for(int i =0;i<n;i++){

			addNode();
		}

		printLL();

		do{

			printf("*----Main Menu----*\n");
			printf("1.StringLength\n");
			printf("2.StringReverse\n");
			printf("3.DeleteString\n");
			printf("4.AddNode\n");
			printf("5.PrintLL\n");

			int choice;

			printf("Enter Your Choice:\n");
			scanf("%d",&choice);

			switch(choice){

				case 1: 
					{
			          		int len;
				  
				  		printf("Enter string length:\n");
				  		scanf("%d",&len);

				  		stringLength(len);
			
					}
						  break;

		       		case 2:
						  stringReverse();
				                  break;

				case 3:
					 {
			               		int len;

                                       		printf("Enter string length:\n");
                                       		scanf("%d",&len);

                                                deleteData(len);

				  	 }
				    		 break;


		       		case 4 : 
				    	         addNode();
				    		 break;

		       		case 5 :
						 printLL();
				   		 break;

		      		default:
				     		printf("Invalid Choice !\n");
				     		break;

			}

			getchar();

			printf("Do You Want to Continue?:\n");
			scanf("%c",&ch);

		}while(ch=='y' || ch == 'Y');


	}else{
		printf("Invalid Node Count!\n");

	}


}









