/*
   Program 1: WAP for the LinkedList of malls consisting of its name,number of shops,&revenue;connect 3 malls in LinkedList & print their data
   
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:40 byte

typedef struct Mall{
	
	char mName[20];
	int nShop;
	double rev;
	struct Mall *next;

}M;

M *head = NULL;

//To add node at end

void addNode(){

   M *newNode = (M*)malloc(sizeof(M));
 
  
  printf("Enter the Mall Name:\n");
  fgets(newNode->mName,20,stdin);
  
  printf("Enter the No of Shops:\n");
  scanf("%d",&(newNode->nShop));
  
  printf("Enter the Revenue:\n");
  scanf("%lf",&(newNode->rev));
  
  getchar();
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  M *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	M *temp = head;
	
	while(temp!=NULL){
		
		printf("|");
		char *str = temp->mName;
		
		while(*str!='\n'){
			
			printf("%c",*str);
			str++;
		}
		
		printf("|%d",temp->nShop);
		
		
		if(temp->next!=NULL)
			
		      printf("|%0.2lf|-->",temp->rev);
        else
              printf("|%0.2lf|",temp->rev);
              
        temp =temp->next;
        
    }
    
    printf("\n");
}


void main(){
	
	// creating LinkedList of three Nodes
	
	for(int i=0;i<3;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();
	
	
}			

	
