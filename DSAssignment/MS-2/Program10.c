/*
   Program 10: WAP a real-time example for linkedlist and print its data
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:40 byte

typedef struct Artist{
	
	char aName[20];
	int nSong;
	float cFee;
	struct Artist *next;

}Art;

Art *head = NULL;

//To add node at end

void addNode(){

   Art *newNode = (Art*)malloc(sizeof(Art));
 
  
  printf("Enter the Artist Name:\n");
  fgets(newNode->aName,20,stdin);
  
  printf("Enter the No of Songs:\n");
  scanf("%d",&(newNode->nSong));
  
  printf("Enter the Consert Fees:\n");
  scanf("%f",&(newNode->cFee));
  
  getchar();
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  Art *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	Art *temp = head;
	
	while(temp!=NULL){
		
		printf("|");
		char *str = temp->aName;
		
		while(*str!='\n'){
			
			printf("%c",*str);
			str++;
		}
		
		printf("|%d",temp->nSong);
		
		
		if(temp->next!=NULL)
			
		      printf("|%0.2f|-->",temp->cFee);
                else
                      printf("|%0.2f|",temp->cFee);
              
        temp =temp->next;
        
    }
    
    printf("\n");
}


void main(){
	
	// creating Nodes of LinkedList
	
	int n;

	printf("Enter the Total no of Node:\n");
	scanf("%d",&n);

	getchar();
	
	for(int i=0;i<n;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();
	
	
}			

	
