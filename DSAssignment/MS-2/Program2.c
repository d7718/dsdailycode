/*
   Program 2: WAP for the LinkedList of States in India consisting of its name,population,Budget,& Literacy.Connect 4 states in the LinkedList & print their data
   
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:56byte

typedef struct India{
	
	char sName[20];
	long int pop;
	double bug;
	float lit;
	struct India *next;

}IND;

IND *head = NULL;

//To add node at end

void addNode(){

   IND *newNode = (IND*)malloc(sizeof(IND));
 
  
  printf("Enter the State Name:\n");
  fgets(newNode->sName,20,stdin);
  
  printf("Enter the Population:\n");
  scanf("%ld",&(newNode->pop));
  
  printf("Enter the Budget:\n");
  scanf("%lf",&(newNode->bug));

  printf("Enter the Literacy Rate:\n");
  scanf("%f",&(newNode->lit));
  
  getchar();
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  IND *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	IND *temp = head;
	
	while(temp!=NULL){
		
		printf("|");
		char *str = temp->sName;
		
		while(*str!='\n'){
			
			printf("%c",*str);
			str++;
		}
		
		printf("|%ld",temp->pop);
		printf("|%0.2lf",temp->bug);
		
		
		if(temp->next!=NULL)
			
		      printf("|%0.2f|-->",temp->lit);
        else
              printf("|%0.2f|",temp->lit);
              
        temp =temp->next;
        
    }
    
    printf("\n");
}


void main(){
	
	// creating LinkedList of three Nodes
	
	for(int i=0;i<3;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();
	
	
}			

	
