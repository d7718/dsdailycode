/*
   Program 3 & 4: WAP for the LinkedList of Festivals in India.Take input from user in Linkedlist and print their data.
                  Also count number of nodes in LinkedList
   
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:40 byte

typedef struct Festival{
	
	char fName[20];
	int nDays;
	float sweetAmt;
	struct Festival *next;

}Fest;

Fest *head = NULL;

//To add node at end

void addNode(){

   Fest *newNode = (Fest*)malloc(sizeof(Fest));
 
  
  printf("Enter the Festival Name:\n");
  fgets(newNode->fName,20,stdin);
  
  printf("Enter the No of Days:\n");
  scanf("%d",&(newNode->nDays));
  
  printf("Enter the Sweet Amount:\n");
  scanf("%f",&(newNode->sweetAmt));
  
  getchar();
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  Fest *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	Fest *temp = head;
	
	while(temp!=NULL){
		
		printf("|");
		char *str = temp->fName;
		
		while(*str!='\n'){
			
			printf("%c",*str);
			str++;
		}
		
		printf("|%d",temp->nDays);
		
		
		if(temp->next!=NULL)
			
		      printf("|%0.2f|-->",temp->sweetAmt);
        else
              printf("|%0.2f|",temp->sweetAmt);
              
        temp =temp->next;
        
    }
    
    printf("\n");
}

// Count No of Nodes in LinkedList

void nodeCount(){

	int cnt =0;
	Fest *temp = head;

	while(temp!=NULL){

		cnt++;
		temp = temp->next;

	}

	printf("Total Node Count:%d\n",cnt);
}



void main(){
	
	// creating nodes of LinkedList
	
	for(int i=0;i<5;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();
	nodeCount();
	
	
}			

	
