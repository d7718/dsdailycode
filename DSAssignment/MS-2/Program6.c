/*
   Program 6: Write a Demo structure consisting of integer data.Take the number of nodes from user and print the addition of first and last node
   
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:16 byte

typedef struct Demo{
	
	int data;
	struct Demo *next;

}D;

D *head = NULL;

//To add node at end

void addNode(){

   D *newNode = (D*)malloc(sizeof(D));
 
  
  printf("Enter the Integer Data:\n");
  scanf("%d",&(newNode->data));
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  D *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	D *temp = head;
	
	while(temp!=NULL){
		
		
		if(temp->next!=NULL)
			
		      printf("|%d|-->",temp->data);
                else
                     printf("|%d|",temp->data);
              
        temp =temp->next;
        
    }


}

// Sum of First and Last Node

void addFirstLast(){

	D * temp = head;
	int sum=0;

	while(temp!=NULL){
		
		if(temp==head || temp->next==NULL){

			sum+=temp->data;
		}

		temp=temp->next;
	}

	printf("\nSum of First And Last node Data:%d\n",sum);
}



void main(){
	
	// creating LinkedList of Nodes
	
	int n;

	printf("Enter total no of nodes:\n");
	scanf("%d",&n);
	
	for(int i=0;i<n;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();

	//Sum of First and Last Node
	
	addFirstLast();
	
	
}			

	
