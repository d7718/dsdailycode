/*
   Program 5: Write a Demo structure consisting of integer data.Take the number of nodes from user and print the addition of integer data
   
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:16 byte

typedef struct Demo{
	
	int data;
	struct Demo *next;

}D;

D *head = NULL;
int sum =0;

//To add node at end

void addNode(){

   D *newNode = (D*)malloc(sizeof(D));
 
  
  printf("Enter the Integer Data:\n");
  scanf("%d",&(newNode->data));

  //Sum of Integer Data

  sum+=newNode->data;
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  D *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	D *temp = head;
	
	while(temp!=NULL){
		
		
		if(temp->next!=NULL)
			
		      printf("|%d|-->",temp->data);
        else
              printf("|%d|",temp->data);
              
        temp =temp->next;
        
    }

	printf("\nSum of Integer Data:%d\n",sum);
}


void main(){
	
	// creating LinkedList of three Nodes
	
	int n;

	printf("Enter total no of nodes:\n");
	scanf("%d",&n);
	
	for(int i=0;i<n;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();
	
	
}			

	
