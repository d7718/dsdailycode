/*
   Program 8: Write a Demo structure consisting of integer data.Take the number of nodes from user and check prime number present in data
   
*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:16 byte

typedef struct Demo{
	
	int data;
	struct Demo *next;

}D;

D *head = NULL;

//To add node at end

void addNode(){

   D *newNode = (D*)malloc(sizeof(D));
 
  
  printf("Enter the Integer Data:\n");
  scanf("%d",&(newNode->data));
  
  newNode->next = NULL;
  
  // If node is first node
  if(head==NULL){
	  
	  head=newNode;
	  
  }	else{
	  
	  D *temp = head;
	  
	  while(temp->next!=NULL){
		  
		  temp=temp->next;
	  }
	  
	  temp->next = newNode;
	  
   }

}

//Print LinkedList

void printLL(){
	
	D *temp = head;
	
	while(temp!=NULL){
		
		
		if(temp->next!=NULL)
			
		      printf("|%d|-->",temp->data);
                else
                     printf("|%d|",temp->data);
              
        temp =temp->next;
        
    }

   printf("\n");


}

//Check Prime Number

void primeNumber(){

	D * temp = head;
	int flag=0,cnt=0;

	while(temp!=NULL){

		flag=0;
		
		if(temp->data==0||temp->data==1){
			flag=1;
		}

		for(int i=2;i<=temp->data/2;i++){

			if(temp->data%i==0){

				flag=1;
		
				break;
			}
		}

		if(flag==0){

			cnt++;
			break;
		}



               temp=temp->next;
	}

	if(cnt==0)
               printf("Prime Number Not Present\n");
	else
               printf("Prime Number Present\n");

}



void main(){
	
	// creating LinkedList of Nodes
	
	int n;

	printf("Enter total no of nodes:\n");
	scanf("%d",&n);
	
	for(int i=0;i<n;i++){
		
		addNode();
		
	}
	
	//Printing LinkedList
	
	printLL();

	//Check Prime Number
	
	primeNumber();
	
	
}			

	
