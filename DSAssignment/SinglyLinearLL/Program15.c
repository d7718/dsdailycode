/*
    Program 15: Write a program that accepts a source singly linear linked list from user and 
                copies the contents into the destination singly linear linked in ascending
                order.

                Input:
                        |110|->|73|-|10|->|80|->|70|->|12|

                Output:
                        |10|->|12|->|70|->|73|->|80|->|110|
*/