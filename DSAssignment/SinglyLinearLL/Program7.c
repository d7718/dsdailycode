/*
    Program 7: Write a program that copies the last N contents of a singly linear source linked list 
               to a destination singly linear linked list.

               Input:
                     Source : |30|->|30|->|70|->|80|->|90|->|100|
                     
                     No:4

              Output:
                     Destination: |70|->|80|->|90|->|100|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;
}Node;


Node *head1 = NULL;
Node *head2 = NULL;


int nodeCount(Node *head){

    int count=0;

    while(head!=NULL){

        count++;
        head = head->next;
    }

    return count;
}

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&newNode->data);

    newNode->next = NULL;

    return newNode;
}


int addNode(Node **head){

    Node *newNode = createNode();

    if(newNode == NULL){

        return -1;
    
    }else{

        if(*head == NULL){

            *head = newNode;
       
        }else{

            Node *temp = *head;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0;
    }
}

int printLL(Node *head){

    if(head==NULL){

        return -1;
    
    }else{

        while(head->next!=NULL){

            printf("|%d|->",head->data);
            head = head->next;
        }

        printf("|%d|\n",head->data);

        return 0;
    }
}

int LNcopy(int cnt){

    int nodeCnt = nodeCount(head1);

    if(nodeCnt>0 && cnt<=nodeCnt){

        Node *src = head1;
        
        while(nodeCnt-cnt){

            src =  src->next;
            nodeCnt--;
        }

        while(src!=NULL){

           Node *dest = (Node*)malloc(sizeof(Node));
           dest->data = src->data;
           dest->next = NULL;

          if(head2==NULL){

            head2 = dest;

          }else{

            Node *temp = head2;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = dest;

           }
           src = src->next;
        } 
        
            return 0;
    }else{

        return -1;
    }
}


void main(){

    int nodeCnt;

    printf("Enter Node Count in LinkedList:\n");
    scanf("%d",&nodeCnt);

    if(nodeCnt>0){

        printf("Elements In LinkedList:\n");

        for(int i=0;i<nodeCnt;i++){

            addNode(&head1);
        }

        printLL(head1);

        printf("Enter No Of Elements:\n");
        scanf("%d",&nodeCnt);

        if(nodeCnt>0){

            LNcopy(nodeCnt);

            printLL(head2);
    
        }
    }
}
