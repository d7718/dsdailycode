/*
    Program 13: Write a program that accepts a source singly linear linked list and
                destination singly linear linked list and check whether source list is a
                sublist of destination list.The function returns the first position at which
                the sub-list is found.

                Input:
                        Source:|73|->|80|->|70|

                        Destination:|10|->|73|->|80|->|17|->|22|->|73|->|80|->|70|->|21|

                Output:
                        First Sub list found at position 6
*/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct Node{

    int data;
    struct Node *next;
}Node;


Node *head1 = NULL;
Node *head2 = NULL;

int nodeCount(Node *head){

    int count=0;

    while(head!=NULL){

        count++;
        head = head->next;
    }

    return count;
}

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&newNode->data);

    newNode->next = NULL;

    return newNode;
}


int addNode(Node **head){

    Node *newNode = createNode();

    if(newNode == NULL){

        return -1;
    
    }else{

        if(*head == NULL){

            *head = newNode;
       
        }else{

            Node *temp = *head;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0;
    }
}

int printLL(Node *head){

    if(head==NULL){

        return -1;
    
    }else{

        while(head->next!=NULL){

            printf("|%d|->",head->data);
            head = head->next;
        }

        printf("|%d|\n",head->data);

        return 0;
    }
}

int sublist(){

    int nodeCnt1 = nodeCount(head1);
    int nodeCnt2 = nodeCount(head2);

    printf("%d %d\n",nodeCnt1,nodeCnt2);


    if(nodeCnt1<= nodeCnt2){

        Node *src = head1;
        Node *dest = head2;
        Node *temp= dest;

        int cnt=0;
        int index=0;

        while(dest!=NULL){

            temp = dest;
            cnt++;

            while(src!=NULL){

                if(temp==NULL){

                    return 0;

                }else if(src->data == temp->data){

                    index = cnt;

                    src = src->next;
                    temp = temp->next;
                }else{

                    break;
                }

            }
                
                if(src == NULL){
                    return index;
                }

                   src = head1;

                dest= dest->next;
            
        }
    }else{

        return -1;
    }
}


void main(){

    int nodeCnt1, nodeCnt2;

    printf("Enter Node Count in LinkedList1:\n");
    scanf("%d",&nodeCnt1);

    printf("Enter Node Count in LinkedList2:\n");
    scanf("%d",&nodeCnt2);

    if(nodeCnt1>0 && nodeCnt2>0){


        printf("Elements In LinkedList1:\n");

        for(int i=0;i<nodeCnt1;i++){

            addNode(&head1);
        }

        printLL(head1);

        printf("Elements In LinkedList2:\n");

        for(int i=0;i<nodeCnt2;i++){

            addNode(&head2);
        }

        printLL(head2);

        int ret = sublist();

        if(ret>0){

            printf("First Sub list found at position %d\n",ret);
        }else if(ret==-1){

            printf("Sublist Large Than Destination\n");

        }else{

            printf("Sublist Not Found!\n");
        }
    }
}




