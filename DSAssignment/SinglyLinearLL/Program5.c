/*
 Program 5 : Write a program that accepts two singly linear linked list from user and also accept 
             range and concat elements of source singly linear linked list from that range after 
             a singly destination linked list.

             Input:
                    Source:|30|->|30|->|70|->|80|->|90|->|100|

                    Destination:|30|->|40|

                    Start:2

                    End:5

            Output:
                    |30|->|40|->|30|->|70|->|80|->|90|
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;
}Node;


Node *head1 = NULL;
Node *head2 = NULL;

int nodeCount(Node *head){

    int count=0;

    while(head!=NULL){

        count++;
        head = head->next;
    }

    return count;
}

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&newNode->data);

    newNode->next = NULL;

    return newNode;
}


int addNode(Node **head){

    Node *newNode = createNode();

    if(newNode == NULL){

        return -1;
    
    }else{

        if(*head == NULL){

            *head = newNode;
       
        }else{

            Node *temp = *head;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0;
    }
}

int printLL(Node *head){

    if(head==NULL){

        return -1;
    
    }else{

        while(head->next!=NULL){

            printf("|%d|->",head->data);
            head = head->next;
        }

        printf("|%d|\n",head->data);

        return 0;
    }
}

int Rconcat(int start ,int end){

    int nodeCnt = nodeCount(head1);

    if((start>0 && start<=end)&&(end>=start && end<=nodeCnt)){

        Node *dest = head2;

        while(dest->next!=NULL){

            dest = dest->next;
        }

        Node *src = head1;
        int cnt = 1;

        while(cnt<=end){

            if(cnt>=start){

                dest->next  = src;
                dest = dest->next;
            }

            cnt++;
             src = src->next;
        }

         dest->next = NULL;

        return 0;
    
    }else{

        return -1;
    }
}


void main(){

    int nodeCnt1, nodeCnt2;

    printf("Enter Node Count in LinkedList1:\n");
    scanf("%d",&nodeCnt1);

    printf("Enter Node Count in LinkedList2:\n");
    scanf("%d",&nodeCnt2);

    if(nodeCnt1>0 && nodeCnt2>0){


        printf("Elements In LinkedList1:\n");

        for(int i=0;i<nodeCnt1;i++){

            addNode(&head1);
        }

        printLL(head1);

        printf("Elements In LinkedList2:\n");

        for(int i=0;i<nodeCnt2;i++){

            addNode(&head2);
        }

        printLL(head2);

        printf("Enter Start And End :\n");
        scanf("%d %d",&nodeCnt1,&nodeCnt2);

        Rconcat(nodeCnt1,nodeCnt2);

        printLL(head2);
    }
}