
/*
   Program 2: Write a program that accepts two singly linear linked list from the user and
              concat source linked list after destination linked list.

              Input:
                    Source: |30|->|30|->|70|
                    
                    Destination : |10|->|20|->|30|->|40|

              Output:
                    |10|->|20|->|30|->|40|->|30|->|30|->|70|
                     
*/

#include<stdio.h>
#include<stdlib.h>


typedef struct Node{

	int data;
	struct Node *next;

}Node;

Node *head1 = NULL;
Node *head2 = NULL;

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&(newNode->data));

    newNode->next = NULL;

    return newNode;

}


int addNode(Node **head){

	Node *newNode = createNode();

    if(newNode==NULL){

        return -1;
    
    }else{

    	if(*head == NULL){

    		*head = newNode;

        }else{
	        
            Node *temp = *head;

	        while(temp->next != NULL){

    		      temp = temp->next;

     	    }

     	    temp->next = newNode;
       }

       return 0;
    }

}

int concat(){

    if(head1 == NULL){

        return -1;
    }else{

        if(head2 == NULL){

            head2 = head1;
        }else{

            Node * temp = head2;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = head1;

        }

        return 0;
    }
}


int printLL(Node *head){

	if(head == NULL){

		return -1;
	
    }else{

		Node *temp = head;

		while(temp->next != NULL){

			printf("|%d|->",temp->data);

			temp = temp->next;
		}

		printf("|%d|\n",temp->data);

        return 0;
	}
}

void main(){

	int nodeCount;

	printf("Enter NodeCount in LinkedList1:\n");
	scanf("%d",&nodeCount);

	for(int i=0;i<nodeCount;i++){

		addNode(&head1);

	}

	printLL(head1);
	
	printf("Enter NodeCount in LinkedList2:\n");
	scanf("%d",&nodeCount);

	for(int i=0;i<nodeCount;i++){

		addNode(&head2);
	}

	printLL(head2);
    concat();
    printLL(head2);


}
