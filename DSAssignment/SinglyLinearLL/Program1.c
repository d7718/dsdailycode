/*
  Program 1 : Write a program that searches all occurrences of a particular element from a 
              singly linear linked list.

              Input : |10|->|20|->|30|->|40|->|30|->|30|->|70|
                      
                      Element:30

              Output : 3
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int no;              // Data element
    struct Node *next;   // Address of next node
}Node;

Node *head = NULL;

Node* createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&newNode->no);

    newNode->next = NULL;
    
    return newNode;
}

int addNode(){

    Node *newNode = createNode();

    if(newNode == NULL){

        return -1;
    
    }else{

        if(head==NULL){

            head = newNode;
        }else{

            Node *temp = head;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0;
    }
}


int printNode(){

    if(head==NULL){

        return -1;
    
    }else{

        Node *temp = head;

        while(temp->next!=NULL){

            printf("|%d|->",temp->no);
            temp = temp->next;
        }

        printf("|%d|\n",temp->no);

        return 0;
    }
}


int nOccurrence(int element){

    Node * temp = head;
    int count = 0;

    while(temp!=NULL){

        if(temp->no == element)

            count++;

        temp = temp->next;
    }

    return count;
}


void main(){

    int totalNode,element;

    printf("Enter Total no of Node:\n");
    scanf("%d",&totalNode);

    if(totalNode>0){

        for(int i=0;i<totalNode;i++){

            addNode();
        }

        printNode();

        printf("Enter Element:\n");
        scanf("%d",&element);

         element = nOccurrence(element);

         if(element==0)

                printf("Output: Element Not Found!\n");
         else
                printf("Output:%d\n",element);
    }else{

          printf("Invalid Node Count!\n");
    }

}