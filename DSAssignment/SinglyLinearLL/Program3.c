/*
  Program 3 : Write a program that accepts two singly linear linked list from user and concat 
              first N elements of the source linked list after destination linked list.

              Input:
                    Source:|30|->|30|->|30|

                    Destination:|10|->|20|->|30|->|40|

                    Number of element: 2

             Output:
                    |10|->|20|->|30|->|40|->|30|->|30|
*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;
}Node;


Node *head1 = NULL;
Node *head2 = NULL;

int nodeCount(Node *head){

    int count=0;

    while(head!=NULL){

        count++;
        head = head->next;
    }

    return count;
}

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&newNode->data);

    newNode->next = NULL;

    return newNode;
}


int addNode(Node **head){

    Node *newNode = createNode();

    if(newNode == NULL){

        return -1;
    
    }else{

        if(*head == NULL){

            *head = newNode;
       
        }else{

            Node *temp = *head;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0;
    }
}

int printLL(Node *head){

    if(head==NULL){

        return -1;
    
    }else{

        while(head->next!=NULL){

            printf("|%d|->",head->data);
            head = head->next;
        }

        printf("|%d|\n",head->data);

        return 0;
    }
}

int Nconcat(int cnt){

    int nodeCnt = nodeCount(head1);

    if(nodeCnt>0 && cnt<=nodeCnt){

        Node *temp1 = head2;

        while(temp1->next!=NULL){

            temp1 = temp1->next;
        }

        Node *temp2 = head1;

        while(cnt){

            temp1->next = temp2;
            temp2 = temp2->next;
            temp1 = temp1->next;
            cnt--;
        }

        temp1->next = NULL;

        return 0;
    
    }else{

        return -1;
    }
}


void main(){

    int nodeCnt1, nodeCnt2;

    printf("Enter Node Count in LinkedList1:\n");
    scanf("%d",&nodeCnt1);

    printf("Enter Node Count in LinkedList2:\n");
    scanf("%d",&nodeCnt2);

    if(nodeCnt1>0 && nodeCnt2>0){


        printf("Elements In LinkedList1:\n");

        for(int i=0;i<nodeCnt1;i++){

            addNode(&head1);
        }

        printLL(head1);

        printf("Elements In LinkedList2:\n");

        for(int i=0;i<nodeCnt2;i++){

            addNode(&head2);
        }

        printLL(head2);

        printf("Enter No Of Elements:\n");
        scanf("%d",&nodeCnt1);

        if(nodeCnt1>0){

            Nconcat(nodeCnt1);

            printLL(head2);
        }
    }
}

