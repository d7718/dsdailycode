/*
 Program 8 : Write a program that copies the contents of the source singly linear linked list to
             the destination singly linear linked list which lies between the particular
             range accepted by the user.
             

             Input:
                    Source:|30|->|30|->|70|->|80|->|90|->|100|

                    Start:2

                    End:5

            Output:
                    Destination :|30|->|70|->|80|->|90|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;
}Node;


Node *head1 = NULL;
Node *head2 = NULL;


int nodeCount(Node *head){

    int count=0;

    while(head!=NULL){

        count++;
        head = head->next;
    }

    return count;
}

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter Data:\n");
    scanf("%d",&newNode->data);

    newNode->next = NULL;

    return newNode;
}


int addNode(Node **head){

    Node *newNode = createNode();

    if(newNode == NULL){

        return -1;
    
    }else{

        if(*head == NULL){

            *head = newNode;
       
        }else{

            Node *temp = *head;

            while(temp->next!=NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0;
    }
}

int printLL(Node *head){

    if(head==NULL){

        return -1;
    
    }else{

        while(head->next!=NULL){

            printf("|%d|->",head->data);
            head = head->next;
        }

        printf("|%d|\n",head->data);

        return 0;
    }
}

int RNcopy(int start,int end){

    int nodeCnt = nodeCount(head1);

    if((start>0 && start<=end)&&(end>=start && end<=nodeCnt)){

        Node *src = head1;
        int cnt = 1;

        while(cnt<=end){

            if(cnt>=start){

                Node *dest = (Node*)malloc(sizeof(Node));
                dest->data = src->data;
                dest->next = NULL;

                if(head2==NULL){

                    head2 = dest;

               }else{

                    Node *temp = head2;

                     while(temp->next!=NULL){

                        temp = temp->next;
                    }

                    temp->next = dest;

               }

            }

             cnt++;
             src = src->next;
        }
          return 0;

    }else{

        return -1;
    }
}


void main(){

    int nodeCnt,nodeCnt1;

    printf("Enter Node Count in LinkedList:\n");
    scanf("%d",&nodeCnt);

    if(nodeCnt>0){

        printf("Elements In LinkedList:\n");

        for(int i=0;i<nodeCnt;i++){

            addNode(&head1);
        }

        printLL(head1);

        printf("Enter Start And End :\n");
        scanf("%d %d",&nodeCnt,&nodeCnt1);

        RNcopy(nodeCnt,nodeCnt1);

        printLL(head2);
    
        
    }
}
