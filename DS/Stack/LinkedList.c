#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;

}Node;

Node *head = NULL;
int size = 0,count=0,flag=0;

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter the Data:\n");
    scanf("%d",&(newNode->data));

    newNode->next = NULL;

    return newNode;
}

int push(){

    count++;

    if(count<=size){

        Node * newNode = createNode();

        if(head==NULL){

            head = newNode;
        
        }else{

            Node *temp = head;

            while(temp->next != NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }

        return 0 ;
    
    }else{

        printf("Stack OverFlow!\n");
        return -1;
    }
}


int pop(){


    if(head == NULL){

        printf("Stack UnderFlow!\n");
        flag=1;
        return -1;
    
    }else{

         int data;
        Node *temp = head;

        if(head->next == NULL){

            data = head->data;
            free(head);
            head = NULL;
            
        }else{

            while(temp->next->next!= NULL){

                temp = temp->next;
            }
        
        data = (temp->next)->data;
        free(temp->next);
        temp->next = NULL;
        
     }
        flag=0;
        return data;
    }
}

int peek(){

    if(head == NULL){

        printf("Nothing to Peek\n");
        return -1;
    
    }else{

        Node *temp = head;

            while(temp->next != NULL){

                temp = temp->next;
            }

            printf("Peeked Element is %d\n",temp->data);
        
        
        return 0;
    }
}


int printStack(){

    if(head == NULL){

        printf("Nothing to Print(Stack UnderFlow!)\n");
        return -1;
    
    }else{

        Node *temp = head;

        while(temp != NULL){

            printf("|%d|",temp->data);
            temp = temp->next;
        }

        printf("\n");

        return 0;
    }
}


void main(){

	printf("Enter the Stack Size:\n");
	scanf("%d",&size);

	if(size>0){

        char ch;


		do{
			printf("\n*----Main Menu----*\n1.Push\n2.Pop\n3.Peek\n4.PrintStack\n");
			
			int choice;

	                printf("Enter the Choice:\n");
			scanf("%d",&choice);

			switch(choice){

				case 1:
					push();
				        break;

				case 2:
					{
				          int ret = pop();

					  if(flag==0)
					        printf("Popped element:%d\n",ret);
					}
					  break;

			        case 3:
					 
					   peek();
					   break;
				           

				case 4:
					printStack();
					break;

				default:
					printf("Invalid Choice!\n");
					break;

			}

			getchar();
			printf("Do you want to continue(Y|N):\n");
			scanf("%c",&ch);

		}while(ch=='Y'||ch=='y');
	
	}else{
		printf("Invalid Stack Size!\n");
	}
}


					  
