#include<stdio.h>

int size=0;
int top1 = 0;
int top2 = 0;
int flag =0;


int push(int *stack ,int toggle){

    if(top1 == top2-1)
        return -1;
    
    else{
    
        if(toggle==0){
            
            top1++;
            printf("Enter Data:\n");
            scanf("%d",&stack[top1]);
        
        }else{

            top2--;
             printf("Enter Data:\n");
             scanf("%d",&stack[top2]);
        }

        return 0;

    }
}


int peek(int *stack, int toggle){

    if(toggle==0){

        if(top1==-1){

            flag=1;
            return-1;
        }else{

            flag =0;
            return stack[top1];
        }
    }else{

        if(top2==size){

            flag=1;
            return-1;
        }else{

            flag =0;
            return stack[top2];
        }
    }

}

int pop(int *stack, int toggle){

    if(toggle==0){

        if(top1==-1){
            flag =1;
           return -1;
        }
        else{

            int data = stack[top1];
            top1--;
            flag = 0;
            return data;
        }
    }else{

         if(top2==size){
            flag =1;
           return -1;
        }
        else{

            int data = stack[top2];
            top2++;
            flag = 0;
            return data;
        }
    }

}


int printStack(int *stack ,int toggle){

       if(toggle==0){

            if(top1==-1)
               return -1;
            
            else{

            for(int i = top1;i>=0;i--){

                printf("|%d|",stack[i]);
            }

            printf("\n");

            }
            
        }else{

            if(top2==size)
                return -1;

            else{

            for(int i = top2;i<size;i++){

                printf("|%d|",stack[i]);
            }
          }

          printf("\n");
    }
       return 0;
}




void main(){

	printf("Enter the Stack Size:\n");
	scanf("%d",&size);

    if(size>0){

		int stack[size];
        top1 = -1;
        top2 = size;
        char ch;


		do{
			printf("\n*----Main Menu----*\n 1.Push1\n 2.Push2\n 3.Pop1\n 4.Pop2\n 5.Peek1\n 6.Peek2\n 7.PrintStack1\n 8.PrintStack2\n");
			
			int choice;

	        printf("Enter the Choice:\n");
			scanf("%d",&choice);

			switch(choice){

				case 1:{
					    int ret = push(stack,0);

                        if(ret==-1)
                            printf("Stack OverFlow!\n");
                      }

				        break;

                case 2:{
					    int ret = push(stack,1);

                        if(ret==-1)
                            printf("Stack OverFlow!\n");
                      }

				        break;
					

				case 3:
					{
				          int ret = pop(stack,0);

					       if(flag==0)
					            printf("Popped element:%d\n",ret);
                           else
                                printf("Stack UnderFlow!\n");
					}
					  break;

                case 4:
					{
				          int ret = pop(stack,1);

					       if(flag==0)
					            printf("Popped element:%d\n",ret);
                           else
                                printf("Stack UnderFlow!\n");
					}
					  break;

			    case 5:
                    {
					     int ret = peek(stack,0);

                            if(flag==0)
					            printf("Peek element:%d\n",ret);
                            else
                                printf("Stack Empty-1!\n");
                    }
					   break;

                case 6:
                    {
					     int ret = peek(stack,1);

                            if(flag==0)
					            printf("Peek element:%d\n",ret);
                            else
                                printf("Stack Empty-2!\n");
                    }
					   break;
				           

				case 7:{
					
                    int ret = printStack(stack,0);

                    if(ret == -1){

                        printf("Stack1 Empty!\n");
                    }
                }
					break;

                case 8:
                {
                    int ret = printStack(stack,1);

                    if(ret == -1){

                        printf("Stack2 Empty!\n");
                    }
                }
					break;

				default:
					printf("Invalid Choice!\n");
					break;

			}

			getchar();
			printf("Do you want to continue(Y|N):\n");
			scanf("%c",&ch);

		}while(ch=='Y'||ch=='y');
	
	}else{
		printf("Invalid Stack Size!\n");
	}
}
