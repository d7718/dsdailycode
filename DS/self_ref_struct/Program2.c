//Program 2

#include<stdio.h>
#include<string.h>

// Self-referential Structure : structure that has pointer which points to structure of same type
typedef struct Batsman{

	int jerNo;
	char bName[20];
	float avg;
	struct Batsman *next;

}Bm;

void main(){

	Bm obj1,obj2,obj3;

	Bm *head = &obj1;
	
	head->jerNo = 7;
	strcpy(head->bName,"MS Dhoni");
	head->avg = 50.35;
	head->next = &obj2;
	
	head->next->jerNo = 10;
	strcpy(head->next->bName,"Sachin Tendulkar");
	head->next->avg = 51.35;
	head->next->next = &obj3;
	
	head->next->next->jerNo = 12;
	strcpy(head->next->next->bName,"Yuvraj Singh");
	head->next->next->avg = 55.34;
	head->next->next->next = NULL;


	printf("**Obj1 Data**\n");
	printf("Jersey No: %d\n",head->jerNo);
	printf("Batsman Name: %s\n",head->bName);
	printf("Average: %f\n",head->avg);
	
	printf("\n**Obj2 Data**\n");
	printf("Jersey No: %d\n",head->next->jerNo);
	printf("Batsman Name: %s\n",head->next->bName);
	printf("Average: %f\n",head->next->avg);

	printf("\n**Obj3 Data**\n");
	printf("Jersey No: %d\n",head->next->next->jerNo);
	printf("Batsman Name: %s\n",head->next->next->bName);
	printf("Average: %f\n",head->next->next->avg);

}
        
