#include<stdio.h>
#include<stdlib.h>

typedef struct Movie{

	char mName[20];
	int tCount;
	float price;
	struct Movie *next;

}MV;

void getData(MV *ptr){

	printf("Enter Movie Name:\n");
	gets(ptr->mName);
	printf("Enter the Ticket Count:\n");
	scanf("%d",&(ptr->tCount));
	printf("Enter the Ticket Price:\n");
	scanf("%f",&(ptr->price));
}

void accessData(MV *ptr){

	printf("Movie Name:%s\n",ptr->mName);
	printf("Ticket Count:%d\n",ptr->tCount);
	printf("Ticket Price:%0.2f\n",ptr->price);
}

void main(){

	MV *m1 =(MV*)malloc(sizeof(MV));
	MV *m2 =(MV*)malloc(sizeof(MV));
	MV *m3 =(MV*)malloc(sizeof(MV));

	m1->next = m2;
	m2->next = m3;
	m3->next = NULL;

	getData(m1);
	accessData(m1);
}
