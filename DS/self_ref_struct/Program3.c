//Program 3

#include<stdio.h>
#include<string.h>

// Self-referential Structure : structure that has pointer which points to structure of same type
typedef struct Company{

	int empNo;
	char cName[20];
	float rev;
	struct Company *next;

}Cmp;

void main(){

	Cmp obj1,obj2,obj3;

	Cmp *head = &obj1;
	
	head->empNo = 1000;
	strcpy(head->cName,"Apple");
	head->rev = 100.15;
	head->next = &obj2;
	
	obj1.next->empNo = 750;
	strcpy(obj1.next->cName,"Google");
	obj1.next->rev = 95.35;
	obj1.next->next = &obj3;
	
	obj2.next->empNo = 650;
	strcpy(obj2.next->cName,"Microsoft");
	obj2.next->rev = 85.45;
	obj2.next->next = NULL;

	printf("**Obj1 Data**\n");
	printf("Employee No: %d\n",head->empNo);
	printf("Company Name: %s\n",head->cName);
	printf("revenue: %f\n",head->rev);
	
	printf("\n**Obj2 Data**\n");
	printf("Employee No: %d\n",obj1.next->empNo);
	printf("Company Name: %s\n",obj1.next->cName);
	printf("revenue: %f\n",obj1.next->rev);
	
	printf("\n**Obj3 Data**\n");
	printf("Employee No: %d\n",obj2.next->empNo);
	printf("Company Name: %s\n",obj2.next->cName);
	printf("revenue: %f\n",obj2.next->rev);

}

        
