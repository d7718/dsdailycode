#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	int priority;
	struct Node *next;
}Node;

Node *front = NULL;
Node *rear = NULL;


Node *createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));

	printf("Enter Data:\n");
	scanf("%d",&newNode->data);

	do{
		printf("Enter Priority(0-5):\n");
		scanf("%d",&newNode->priority);

	}while(newNode->priority<0 || newNode->priority>5);


	newNode->next= NULL;

	return newNode;

}

void addAtPos(Node *newNode ,int pos){

	Node *temp =  front;

	while(pos-2){

		pos--;
		temp = temp->next;
	}

	newNode->next = temp->next;
	temp->next = newNode;
}
void sort(){

    	Node *newNode = createNode();

        Node *temp =  front;

	     int index=0;

	     if(front==NULL){

	          front = newNode;
			  rear = newNode;
			  return;
	     
		 }else if(front->next==NULL){

			if(newNode->priority>temp->priority){

				  newNode->next = front;
				  rear = front;
				  front = newNode;
			}else{

				  front->next = newNode;
				  rear = newNode;
			}
		  
		  }else{

			 if(newNode->priority>temp->priority){

				  newNode->next = front;
				  rear = front;
				  front = newNode;
			
		  	  }else{
				
				while(temp->next!=NULL){

				index++;

		        if(newNode->priority<temp->priority && newNode->priority>temp->next->priority){

                    addAtPos(newNode,index+1);
					return;
		       }
			 
		      temp = temp->next;
			   

	        }

			addAtPos(newNode,index+2);
		   }
      }

}
	


void printQueue(){

	if(front==NULL){

		printf("QUEUE EMPTY\n");
	}else{
		Node *temp = front;

		while(temp!=NULL){

			printf("|%d|",temp->data);
			temp = temp->next;
		}
		printf("\n");
	}
}


void deleteFirst(){

	if(front==NULL){

		printf("QUEUE UNDEFLOW!\n");

	}else{
		int val = front->data;

		if(front->next==NULL){

			free(front);
			front =NULL;
			rear =NULL;

		}else{
			Node *temp = front;
			front = front->next;
			free(temp);
		}
	}
}


void main(){

	int choice;
	char ch;

	do{
		printf("\n*----PRIORITY QUEUE----*\n");
		printf("1.ENQUEUE\n");
		printf("2.DEQUEUE\n");
		printf("3.PRINTQUEUE\n");

		printf("Enter Choice:\n");
		scanf("%d",&choice);

		switch(choice){

			case 1:
				sort();
				break;

			case 2:
				deleteFirst();
				break;

			case 3:
				printQueue();
				break;

			default:
				printf("Invalid Choice\n");
				break;

	       }

	       getchar();
	       printf("Do you want to continue:\n");
	       scanf("%c",&ch);

	 }while(ch=='y' || ch == 'Y');

}

	


