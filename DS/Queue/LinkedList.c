#include<stdio.h>

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;

}Node;

Node *head = NULL;
int size = 0,count=0,flag=0;

Node *createNode(){

    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter the Data:\n");
    scanf("%d",&(newNode->data));

    newNode->next = NULL;

    return newNode;
}

void addNode(){

    Node * newNode = createNode();

        if(head==NULL){

            head = newNode;
        
        }else{

            Node *temp = head;

            while(temp->next != NULL){

                temp = temp->next;
            }

            temp->next = newNode;
        }
}


int deletefirst(){

    if(head == NULL){
  
        return -1;

    }else{

        int data;

        if(head->next == NULL){

            data = head->data;
            free(head);
            head = NULL;
        }else{

            Node *temp = head;
            data =  head->data;
            head = head->next;
            free(temp);
        }
        return data;;
    }
}

int enqueue(){

    count++;
    printf("C=%d\n",count);

    if(count>size){
        count--;
       return -1;
    }
    else{
    
        addNode();
        return 0;

    }
}


int dequeue(){

    if(head == NULL){

        flag =1;
        return -1;
    }else{

        int data = deletefirst();
        flag = 0;
        count--;
        printf("C1=%d\n",count);
        return data;
    }
}

int frontt(){

     if(head == NULL){

        flag =1;
        return -1;
    }else{

        flag = 0;
        return head->data;
    }
}


int printQueue(){

    if(head == NULL)
       return -1;
    else{

        Node *temp = head;

        while(temp != NULL){

            printf("|%d|",temp->data);
            temp = temp->next;
        }

        return 0;
    }
}



void main(){

	printf("Enter the Queue Size:\n");
	scanf("%d",&size);

	if(size>0){

        char ch;


		do{
			printf("\n*----Main Menu----*\n 1.Enqueue\n 2.Dequeue\n 3.front\n 4.PrintQueue\n");
			
			int choice;

	        printf("Enter the Choice:\n");
			scanf("%d",&choice);

			switch(choice){

				case 1:{

                      int ret = enqueue();

                      if(ret==-1){

                        printf("Queue OverFlow!\n");
                      }

                    }
					
				        break;

				case 2:
					{
				          int ret = dequeue();

					  if(flag==0)
					        printf("Dequeued element:%d\n",ret);
                    
                      else
                           printf("Queue UnderFlow!\n");
					}
					  break;

			        case 3:
					 
					{
                         int ret = frontt();

					    if(flag==0)
					        printf(" front element:%d\n",ret);

                        else
                            printf("Queue Empty!\n");
                    }
					   break;
				           

				case 4:
                {
                    int ret = printQueue();

                    if(ret==-1)
                        printf("Queue Empty!\n");

    
                 }
				
					break;

				default:
					printf("Invalid Choice!\n");
					break;

			}

			getchar();
			printf("Do you want to continue(Y|N):\n");
			scanf("%c",&ch);

		}while(ch=='Y'||ch=='y');
	
	}else{
		printf("Invalid Stack Size!\n");
	}
}

