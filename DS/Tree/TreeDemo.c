#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

    int data;
    struct Node *left;
    struct Node *right;
}Node;


Node * head = NULL;

int preOrder(Node* root){

    if(root== NULL)
        return 0;

    printf("%d ",root->data);
    preOrder(root->left);
    preOrder(root->right);
}


int inOrder(Node* root){

    if(root== NULL)
        return 0;

    inOrder(root->left);
    printf("%d ",root->data);
    inOrder(root->right);
}


int postOrder(Node *root){

    if(root== NULL)
        return 0;
    
    postOrder(root->left);
    postOrder(root->right);
    printf("%d ",root->data);

}

Node* createNode(){

    Node *newNode = malloc(sizeof(Node));

    printf("Enter Data:");
    scanf("%d",&newNode->data);
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;


}

Node* createTree(){

    Node *newNode = createNode();
    int left,right;

    if(head==NULL)
        head = newNode;

    printf("Do you want to add left child(y-1 | n -0)?:");
    scanf("%d",&left);

    if(left==1){

           newNode->left = createTree();
    }

    printf("Do you want to add right child(y-1 | n -0)?:");
    scanf("%d",&right);

    if(right==1){

        newNode->right= createTree();
    }

    return newNode;

}



void main(){


    createTree();

    printf("PreOrder: ");
    preOrder(head);

    printf("\nInOrder: ");
    inOrder(head);

    printf("\nPostOrder: ");
    postOrder(head);

    /*printf("%d\n",head->data);
    printf("%d\n",head->left->data);
    printf("%d\n",head->right->data);
*/
    

    
}