#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode{

    int data;
    struct TreeNode *left;
    struct TreeNode *right;
}TreeNode;


TreeNode * head = NULL;

int preOrder(TreeNode* root){

    if(root== NULL)
        return 0;

    printf("%d ",root->data);
    preOrder(root->left);
    preOrder(root->right);
}


int inOrder(TreeNode* root){

    if(root== NULL)
        return 0;

    inOrder(root->left);
    printf("%d ",root->data);
    inOrder(root->right);
}


int postOrder(TreeNode *root){

    if(root== NULL)
        return 0;
    
    postOrder(root->left);
    postOrder(root->right);
    printf("%d ",root->data);

}


TreeNode* createTreeNode(int level){

    level = level+1;

    TreeNode *newNode = (TreeNode*)malloc(sizeof(TreeNode));

    printf("Enter Data for Node:\n");
    scanf("%d",&(newNode->data));

    char ch;

    getchar();
    printf("Do you want to construct left binary tree for level:%d\n",level);
    scanf("%c",&ch);

    if(ch=='y'||ch=='Y')
        newNode->left = createTreeNode(level);
    else
        newNode->left = NULL;

    

    getchar();
    printf("Do you want to construct right binary tree for level:%d\n",level);
    scanf("%c",&ch);


    if(ch=='y'||ch=='Y')
        newNode->right = createTreeNode(level);
    else
        newNode->right = NULL;

    
    return newNode;



}

void printTree(TreeNode *root){

    char ch;

    do{

        printf("\t*------Traversals-------*\n");
        printf("1.Preorder\n");
        printf("2.Inorder\n");
        printf("3.Postorder\n");

        int choice;

        printf("Enter Your Choice:\n");
        scanf("%d",&choice);

        switch(choice){

            case 1: printf("Preorder Traversal: ");
                    preOrder(root);
                    break;

            case 2: printf("Inorder Traversal: ");
                    inOrder(root);
                    break;

            case 3: printf("Postorder Traversal: ");
                    postOrder(root);
                    break;

            default:printf("Invalid Choice\n");
                    break;
        }

        getchar();
        printf("\nDo you want to continue:\n");
        scanf("%c",&ch);
    }while(ch=='y'||ch=='Y');
}


int sizeOfBT(TreeNode* root){

    if(root==NULL)
        return 0;

    int leftSize = sizeOfBT(root->left);
    int rightSize = sizeOfBT(root->right);

    return leftSize+rightSize+1;
}

int sumOfBT(TreeNode* root){

    if(root==NULL)
        return 0;
    
    int leftSum = sumOfBT(root->left);
    int rightSum = sumOfBT(root->right);

    return leftSum+rightSum+root->data;
}


int maxOf(int leftHeight,int rightHeight){

    if(leftHeight>rightHeight)
        return leftHeight;
    else
        return rightHeight;
}


int heightOfBT(TreeNode* root){

    if(root==NULL)
        return -1;

    int leftHeight = heightOfBT(root->left);
    int rightHeight = heightOfBT(root->right);

    return maxOf(leftHeight,rightHeight)+1;
}



int sumOfSkipBT(TreeNode *root){

    if(root==NULL)
        return 0;

    int rightSum,leftSum;

    if(root->left!=NULL && root->right!=NULL){
       
       leftSum = sumOfSkipBT(root->left);
       return leftSum+root->data;
              
   }else if(root->left!=NULL){

       leftSum = sumOfSkipBT(root->left);
       return leftSum+root->data;
  
   }else{
      
       rightSum = sumOfSkipBT(root->right);
       return rightSum+root->data;
   }

}



void main(){

    TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));

    printf("*\t------Creating Binary Tree------*\n");

    printf("Enter Data for Root Node:\n");
    scanf("%d",&(root->data));

    printf("Tree constructed with root : %d\n",root->data);

    char ch;

    getchar();
    printf("Do you want to construct left binary tree for root: \n");
    scanf("%c",&ch);

    if(ch=='y'||ch=='Y')
        root->left = createTreeNode(0);
    else
        root->left = NULL;

    

    getchar();
    printf("Do you want to construct right binary tree for root: \n");
    scanf("%c",&ch);


    if(ch=='y'||ch=='Y')
        root->right = createTreeNode(0);
    else
        root->right = NULL;

    
    printTree(root);

    printf("Size of Binary Tree: %d\n",sizeOfBT(root));
    printf("Sum of nodes in Binary Tree: %d\n",sumOfBT(root));
    printf("Height of Binary Tree: %d\n",heightOfBT(root));
    printf("Sum of Skip Node Binary Tree: %d\n",sumOfSkipBT(root));

}



