#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct TreeNode{

    int data;
    struct TreeNode *left;
    struct TreeNode *right;
}TreeNode;


typedef struct StackFrame{

    TreeNode *btNode;
    struct StackFrame *next;
}StackFrame;


StackFrame *top = NULL;

bool isEmpty(){

    if(top==NULL)
        return true;

    return false;
}

void push(TreeNode *temp){

    StackFrame *newNode = (StackFrame*)malloc(sizeof(StackFrame));
   
    newNode->btNode = temp;
    newNode->next = top;
    top =  newNode;

}


TreeNode* pop(){

    if(isEmpty()){

        printf("Tree is Empty!\n");

    }else{
    
        TreeNode *item = top->btNode;
        
        StackFrame *temp = top;
        top =  top->next;
        free(temp);

        return item;
   }

}

void iterativeInorder(TreeNode *temp){

    while(!isEmpty()||temp!=NULL){

        if(temp!=NULL){

            push(temp);
            temp =  temp->left;

        }else{

            temp = pop();
            printf("%d ",temp->data);
            temp = temp->right;
        }


    }
}


int inOrder(TreeNode* root){

    if(root== NULL)
        return 0;

    inOrder(root->left);
    printf("%d ",root->data);
    inOrder(root->right);
}


TreeNode* createTreeNode(int level){

    level = level+1;

    TreeNode *newNode = (TreeNode*)malloc(sizeof(TreeNode));

    printf("Enter Data for Node:\n");
    scanf("%d",&(newNode->data));

    char ch;

    getchar();
    printf("Do you want to construct left binary tree for level:%d\n",level);
    scanf("%c",&ch);

    if(ch=='y'||ch=='Y')
        newNode->left = createTreeNode(level);
    else
        newNode->left = NULL;

    

    getchar();
    printf("Do you want to construct right binary tree for level:%d\n",level);
    scanf("%c",&ch);


    if(ch=='y'||ch=='Y')
        newNode->right = createTreeNode(level);
    else
        newNode->right = NULL;

    
    return newNode;



}

void printTree(TreeNode *root){

    char ch;

    do{

        printf("\t*------Traversals-------*\n");
        printf("1.Inorder\n");
        printf("2.Iterative Inorder\n");

        int choice;

        printf("Enter Your Choice:\n");
        scanf("%d",&choice);

        switch(choice){


            case 1: printf("Inorder Traversal: ");
                    inOrder(root);
                    break;

            case 2: printf("Iterative Inorder Traversal: ");
                    iterativeInorder(root);
		    break;
          

            default:printf("Invalid Choice\n");
                    break;
        }

        getchar();
        printf("\nDo you want to continue:\n");
        scanf("%c",&ch);
    }while(ch=='y'||ch=='Y');
}





void main(){

    TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));

    printf("*\t------Creating Binary Tree------*\n");

    printf("Enter Data for Root Node:\n");
    scanf("%d",&(root->data));

    printf("Tree constructed with root : %d\n",root->data);

    char ch;

    getchar();
    printf("Do you want to construct left binary tree for root: \n");
    scanf("%c",&ch);

    if(ch=='y'||ch=='Y')
        root->left = createTreeNode(0);
    else
        root->left = NULL;

    

    getchar();
    printf("Do you want to construct right binary tree for root: \n");
    scanf("%c",&ch);


    if(ch=='y'||ch=='Y')
        root->right = createTreeNode(0);
    else
        root->right = NULL;

    
    printTree(root);

  
}



