#include<stdio.h>

void rightMaxArray(int arr[],int N){

    int rightMaxArr[N];

    rightMaxArr[N-1] = arr[N-1];

    for(int i =N-2;i>=0;i--){

        if(arr[i]>rightMaxArr[i+1])
            rightMaxArr[i] = arr[i];
        else
            rightMaxArr[i] = rightMaxArr[i+1];
    }

    for(int i=0;i<N;i++){

        printf("%d ",rightMaxArr[i]);
    }

    printf("\n");
}


void main(){

    int  arr[] = {5,2,1,-4,-2,9,3,4,7};

    int N = sizeof(arr)/sizeof(int);

    rightMaxArray(arr,N);
}