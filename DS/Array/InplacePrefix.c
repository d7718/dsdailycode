
#include<stdio.h>

void inplacePrefix(int arr[],int N){

    for(int i=1;i<N;i++){

        arr[i] = arr[i-1]+arr[i];
    }

     
}

void main(){

    int arr[] = {-7,11,21,-4,-3,5,8,2,-12};

    int N = sizeof(arr)/sizeof(int);

    inplacePrefix(arr,N);

    for(int i=0;i<N;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");

}