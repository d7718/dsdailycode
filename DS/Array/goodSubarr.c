#include<stdio.h>


int goodSubarr(int arr[],int N,int B){

    int cnt = 0;

    for(int i=0;i<N;i++){

        int sum=0,size=0;

        for(int j=i;j<N;j++){

            size++;

            sum+=arr[j];

            if((sum<B && size%2==0) || (sum>B && size%2 !=0)){

                cnt++;
            }
        }
    }

    printf("Good SubArray:%d\n",cnt);

    return cnt;
}


void main(){

    int arr[] = {1,2,3,4,5};

    int N = sizeof(arr)/sizeof(int);

    int B = 4;

    goodSubarr(arr,N,B);
}