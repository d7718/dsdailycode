#include<stdio.h>


void printSubarr_approach1(int arr[],int N){


    for(int i=0;i<N;i++){

        for(int j=i;j<N;j++){

            for(int k=i;k<=j;k++){

                printf("%d ",arr[k]);
            }

            printf("\n");
        }
    }
}



void main(){

    int arr[] = {2,4,1,3};

    int N = sizeof(arr)/sizeof(int);


    printf("Approach 1\n");

    printSubarr_approach1(arr,N);

    
}

