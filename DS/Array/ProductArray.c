#include<stdio.h>

void productArray(int arr[],int N){

    int product = 1;

    for(int i=0;i<N;i++){

        product*=arr[i];
    }

    int productArr[N];

    for(int i=0;i<N;i++){

        productArr[i] = product/arr[i];
    }

    for(int i=0;i<N;i++){

         printf("%d ",productArr[i]);
    }
    printf("\n");
}


void main(){

    int arr[] = {5,1,10,1};

    int N = sizeof(arr)/sizeof(int);

    productArray(arr,N);

}
