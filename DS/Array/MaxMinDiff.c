#include<stdio.h>

int maxMinDiff(int arr[],int len){

    int eMax = arr[0];
    int oMin = arr[0];


    for(int i=0;i<len;i++){

        if(arr[i]%2==0 && arr[i]>eMax)
                eMax = arr[i];
        
        else{

            if(arr[i]<oMin){

                oMin = arr[i];
            }
        }
   }

    return eMax-oMin;
}

void main(){

    int size;

    printf("Enter Size of array:\n");
    scanf("%d",&size);


    if(size>0){

        int arr[size];

        printf("Enter Elements of Array:\n");

        for(int i =0;i< size;i++){

            scanf("%d",&arr[i]);
         }

        
        printf("Difference Between Even Max and Min Odd is %d\n",maxMinDiff(arr,size));

    }else{

        printf("Invalid array size!\n");
    }

}