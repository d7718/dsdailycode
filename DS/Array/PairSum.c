#include<stdio.h>

int pairSum(int arr[],int len,int sum){

    int cnt=0;

    for(int i=0;i<len-1;i++){

        for(int j=i+1;j<len;j++){

            if(arr[i]+arr[j]==sum)
                    cnt++;
            
        }
    }

    return cnt;
}


void main(){

    int size;

    printf("Enter Size of array:\n");
    scanf("%d",&size);


    if(size>0){

        int arr[size];

        printf("Enter Elements of Array:\n");

        for(int i =0;i< size;i++){

            scanf("%d",&arr[i]);
         }

         int sum;

         printf("Enter Sum:\n");
         scanf("%d",&sum);

        printf("Count of Pair-Sum is %d\n",pairSum(arr,size,sum));

    }else{

        printf("Invalid array size!\n");
    }

}