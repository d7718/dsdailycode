#include<stdio.h>

void prefixArray(int arr[],int N,int queries){

    int prefixArr[N];

    prefixArr[0] = arr[0];

    for(int i=1;i<N;i++){

        prefixArr[i] = prefixArr[i-1]+arr[i];
    }

    int start,end;

    for(int i=0;i<queries;i++){

        printf("Enter Start and End:\n");
        scanf("%d %d",&start,&end);

        if(start>=0 && start<=end && end<N ){

            if(start==0)
                printf("Sum:%d\n",prefixArr[end]);
            else
                printf("Sum:%d\n",prefixArr[end]-prefixArr[start-1]);
        }

    }

    
}


void main(){

    int arr[] = {-7,11,21,-4,-3,5,8,2,-12};

    int N = sizeof(arr)/sizeof(int);

    int queries;

    printf("Enter No of Queries:\n");
    scanf("%d",&queries);

    prefixArray(arr,N,queries);
}