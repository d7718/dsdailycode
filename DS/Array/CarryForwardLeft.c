#include<stdio.h>

void leftMaxArray(int arr[],int N){

    int leftMaxArr[N];

    leftMaxArr[0] = arr[0];

    for(int i =1;i<N;i++){

        if(arr[i]>leftMaxArr[i-1])
            leftMaxArr[i] = arr[i];
        else
            leftMaxArr[i] = leftMaxArr[i-1];
    }

    for(int i=0;i<N;i++){

        printf("%d ",leftMaxArr[i]);
    }

    printf("\n");
}


void main(){

    int  arr[] = {5,2,1,-4,-2,9,3,4,7};

    int N = sizeof(arr)/sizeof(int);

    leftMaxArray(arr,N);
}