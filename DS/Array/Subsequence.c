#include<stdio.h>

int subSequence(char sArr[],int N){

    int cnt=0;

   
    for(int i=0;i<N;i++){

        for(int j=0;j<N;j++){

            if(sArr[i]=='A'&& sArr[j]=='G'&& i<j){

                cnt++;
                //printf("i:%d j:%d\n",i,j);

            }
               
        }
    }

    return cnt;
}

void main(){

    char sArr[] = "GAB";

    int N = sizeof(sArr);


    int ret = subSequence(sArr,N-1);

    printf("Count:%d\n",ret);
}