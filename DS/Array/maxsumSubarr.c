#include<stdio.h>


// Approach_1: Brute force approach_1:

int maxsumSubarr_Approach1(int arr[],int N){

    int maxSum = arr[0];

    for(int i=0;i<N;i++){

        for(int j=i;j<N;j++){

            int sum=0;

            for(int k=i;k<j;k++){

                sum+= arr[k];
            }

            if(maxSum<sum)
                maxSum = sum;
        }
    }

    printf(" MaxSum:%d\n",maxSum);
    
    return maxSum;
}


// Approach_2 : Brute force approach_2

int maxsumSubarr_Approach2(int arr[],int N){

    int maxSum = 0;

    for(int i=0;i<N;i++){

        int sum =0;

        for(int j=i;j<N;j++){

            sum+=arr[j];

            if(maxSum<sum)
                maxSum = sum;
        }
    }

    printf(" MaxSum:%d\n",maxSum);

    return maxSum;
}


// Approach_3 : Using Kadane's Algorithm

int maxsumSubarr_Approach3(int arr[],int N){

    int maxSum = arr[0],sum = 0;

    for(int i=0;i<N;i++){

        sum += arr[i];

        if(sum < 0 )
            sum =0;

        if(maxSum<sum)
            maxSum = sum;
    }

    printf(" MaxSum:%d\n",maxSum);

    return maxSum;
}


void main(){

    int arr[] = {-2,1,-3,4,-1,2,1,-5,4};

    int N = sizeof(arr)/sizeof(int);

    printf("Approach_1:");

    maxsumSubarr_Approach1(arr,N);

    printf("Approach_2:");

    maxsumSubarr_Approach2(arr,N);

    printf("Approach_3:");

    maxsumSubarr_Approach3(arr,N);
}