#include<stdbool.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>


bool isPrime(int num){

    if(num==0 || num==1)
            return false;

    for(int i=2;i<=num/2;i++){

        if(num%i==0)
             return false;

    }

    return true;
}

int mysqrt(int num){

	 int start =0,end= num, mid;

	 float ans;

	 while(start<=end){

		 mid = (start+end)/2;

		 if(mid*mid == num){

			 ans = mid;
			 break;
		}

		if(mid*mid<num){

			ans = start;

			start = mid+1;
		}else{

			end= mid-1;
		}
	   }

		float incre = 0.1;

		for(int i=0;i<5;i++){

			while(ans*ans<=num){

				ans+=incre;
			}

			ans = ans-incre;

			incre = incre/10;
		}

		return ans;
}
		
bool isPrime1(int num){

    if(num==0 || num==1)
            return false;

    for(int i=2;i<=mysqrt(num);i++){

        if(num%i==0)
             return false;

    }

    return true;
}




void main(){


    int num ;

    printf("Enter Number:\n");
    scanf("%d",&num);

    if(num>=0){

        if(isPrime1(num))
            printf("%d is Prime Number\n",num);
        else
            printf("%d is Not Prime Number\n",num);
    }
}
