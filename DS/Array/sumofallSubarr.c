#include<stdio.h>


int sumofallSubarr(int arr[],int N){

    int tsum=0;

    for(int i=0;i<N;i++){

         int sum =0;

        for(int j=i;j<N;j++){

            sum += arr[j];
        
            tsum +=sum;
        }
    }

    printf("Sum Of All SubArray:%d\n",tsum);

    return tsum;
}


void main(){

    int arr[] = {1,2,3};

    int N = sizeof(arr)/sizeof(int);

    sumofallSubarr(arr,N);
}