#include<stdio.h>

int equilibriumIndex(int arr[],int N){

    int prefixArr[N];

    prefixArr[0] = arr[0];

    for(int i=1;i<N;i++){

        prefixArr[i] = prefixArr[i-1]+arr[i];
    }

    for(int i=1;i<N;i++){

        if (prefixArr[i-1] == prefixArr[N]-prefixArr[i])
             return i;
    }

    return -1;
}


void main(){

    int arr[] = {-7,1,5,2,-4,3,0};

    int N = sizeof(arr)/sizeof(int);

    int ret = equilibriumIndex(arr,N);

    printf("Equilibrium Index:%d\n",ret);

}