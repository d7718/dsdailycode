#include<stdio.h>


// Approach_1 : Brute Force Approach_1

void sumofSubarr_Approach1(int arr[],int N){

    for(int i=0;i<N;i++){

        for(int j=i;j<N;j++){

            int sum=0;

            for(int k=i;k<=j;k++){

                sum+=arr[k];
            }

            printf("%d\n",sum);
        }
    }
}


// Approach_2 : Brute Force Approach_2 

void sumofSubarr_Approach2(int arr[],int N){

    for(int i=0;i<N;i++){

        int sum =0;

        for(int j=i;j<N;j++){

            sum += arr[j];
            printf("%d\n",sum);


        }
    }
}

// Approach_3 : Using Prefix sum array

void sumofSubarr_Approach3(int arr[],int N){

    int pSum[N];

    pSum[0] = arr[0];

    for(int i = 1;i<N;i++){

        pSum[i] = pSum[i-1]+arr[i];
    }


    for(int i=0;i<N;i++){

        int sum=0;

        for(int j =i;j<N;j++){

            if(i==0){

                sum = pSum[j];
            
            }else{

                sum = pSum[j]-pSum[i-1];
            }

            printf("%d\n",sum);
        }
    }

}

void main(){

    int arr[] = {2,4,1,3};

    int N = sizeof(arr)/sizeof(int);

    printf("Approach_1:\n");

    sumofSubarr_Approach1(arr,N);

    printf("Approach_2:\n");

    sumofSubarr_Approach2(arr,N);

    printf("Approach_3:\n");

    sumofSubarr_Approach3(arr,N);
}