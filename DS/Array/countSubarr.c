#include<stdio.h>

int countSubarr(int arr[],int N,int B){

    int  cnt =0;

    for(int i =0;i<N;i++){

        int sum =0 ;

        for(int j=i;j<N;j++){

            sum+= arr[j];

            if(sum<B)
                cnt++;
        }
    }

    printf("Count of SubArray:%d\n",cnt);

    return cnt;
}


void main(){

    int arr[] = {1,11,2,3,15};

    int N = sizeof(arr)/sizeof(int);

    int B = 10;

    countSubarr(arr,N,B);

    
}