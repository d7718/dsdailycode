#include<stdio.h>
#include<stdbool.h>

int mystrlen(char* str){

    int cnt=0;

    while(*str!='\0'){

        str++;
        cnt++;
    }

    return cnt;
}

bool stringCompare(char* str1,char* str2,int len){

    if(len==0)
        return true;
    
    return (str1[len-1]==str2[len-1] && stringCompare(str1,str2,len-1));
}


void main(){

    char str1[100],str2[100];

    printf("Enter Two Strings:\n");
    gets(str1);
    gets(str2);

    if(mystrlen(str1)== mystrlen(str2)){


        bool ret = stringCompare(str1,str2,mystrlen(str1));

        if(ret)
          printf("Output:Strings are equal\n");
        else
          printf("Output:Strings are not equal\n");
        
    }else{

         printf("Output:Strings are not equal\n");

    }
}