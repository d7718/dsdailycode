#include<stdio.h>

int countZero(int num){

    if(num==0)
        return 0;
    
    if(num%10==0)
        return 1+countZero(num/10);

    
    return countZero(num/10);
}


void main(){

    int num;

    printf("Enter Number:\n");
    scanf("%d",&num);

    if(num==0){

        printf("Output:%d\n",1);
    }else{
    
        int ret = countZero(num);
        printf("Output:%d\n",ret);
    }
}