#include<stdio.h>

 int search(char arr[],int len,char ch){
   
    if(len<1)
        return -1;

    if(arr[len-1] == ch)
        return len-1;

      return search(arr,len-1,ch);
 }


 void main(){

    char arr[] = {'A','B','C','D','E'};

    int ret = search(arr,1,'B');

    printf("%d\n",ret);
 }