#include<stdio.h>

int fibo(int n){

    if(n<=1){

        if(n==0)
            return 0;
        
        return 1;
    }

    return fibo(n-1)+fibo(n-2);
}


void main(){

    int ret = fibo(4);

    printf("%d\n",ret);
}