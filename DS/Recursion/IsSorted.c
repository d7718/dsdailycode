#include<stdio.h>
#include<stdbool.h>

bool isSorted(int arr[],int index,int len){

    if(index == len-1)
        return true;

    return arr[index]<=arr[index+1] && isSorted(arr,index+1,len);
}


void main(){

    int len;

    printf("Enter length:\n");
    scanf("%d",&len);

    if(len>0){

        int arr[len];

        printf("Enter Array Element:\n");
        
        for(int i=0;i<len;i++){

            scanf("%d",&arr[i]);
        }

        bool ret = isSorted(arr,0,len);
    
        if(ret)
          printf("Output:Sorted Array\n");
        else
          printf("Output:Not Sorted Array\n");
   
    }
}