#include<stdio.h>

int countStep(int num){

    if(num==0)
        return 0;

    if(num%2==0)
       return 1+countStep(num/2);

    return 1+countStep(num-1);
}

void main(){

    int num;

    printf("Enter Number:\n");
    scanf("%d",&num);

    int ret = countStep(num);
    printf("Output:%d\n",ret);
}