#include<stdio.h>

int factNum(int num){

    if(num<=1)
       return 1;

    return factNum(num-1)*num;
}


void main(){

    int fact =  factNum(5);
    
    printf("%d\n",fact);
}