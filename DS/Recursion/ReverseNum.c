#include<stdio.h>

int reverseNum(int num){

    if(num==0)
     return 0;

    return num%10+reverseNum(num/10);
}

void main(){

    int num;

    printf("Enter Number:\n");
    scanf("%d",&num);

    if(num<10){

        printf("Output:%d\n",num);
    }else{
    
        int ret = reverseNum(num);
        printf("Output:%d\n",ret);
    }
}
