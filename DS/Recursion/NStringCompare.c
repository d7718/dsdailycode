#include<stdio.h>
#include<stdbool.h>

int mystrlen(char* str){

    int cnt=0;

    while(*str!='\0'){

        str++;
        cnt++;
    }

    return cnt;
}

bool nStringCompare(char* str1,char* str2,int index,int n){

    if(index==n-1)
        return true;
    
    return (str1[index]==str2[index] && nStringCompare(str1,str2,index+1,n));
}


void main(){

    char str1[100],str2[100];

    printf("Enter Two Strings:\n");
    gets(str1);
    gets(str2);

    int min =0;

    if(mystrlen(str1)<=mystrlen((str2)))
        min = mystrlen(str1);
    else
       min = mystrlen(str2);

    int n;

    printf("Enter n:\n");
    scanf("%d",&n);

    if(n<= min){


        bool ret = nStringCompare(str1,str2,0,n);

        if(ret)
          printf("Output:Strings are equal\n");
        else
          printf("Output:Strings are not equal\n");
        
    }else{

         printf("Output:Invalid N \n");

    }
}