#include<stdio.h>

int arraySum(int arr[],int len){

    if(len==1)
        return arr[len-1];

    return arr[len-1]+arraySum(arr,len-1);
}


void main(){

    int arr[] = {1,2,3,4,5};

    int ret  = arraySum(arr,5);

    printf("%d\n",ret);
}