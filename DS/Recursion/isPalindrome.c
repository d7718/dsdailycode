#include<stdio.h>
#include<stdbool.h>

bool isPalindrome(char arr[],int start,int end){

    if(start>=end)
        return true;

    if(arr[start] != arr[end-1])
        return false;

    return isPalindrome(arr,start+1,end-1);
}


void main(){

    char *arr = "madam";

    bool ret = isPalindrome(arr,0,5);

    printf("%d\n",ret);

    if(ret)
        printf("True\n");
    else
        printf("False\n");
}