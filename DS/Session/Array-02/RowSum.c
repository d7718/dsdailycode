/*

    Row Sum:
*/


#include<stdio.h>

int cols=0;

int rowsSum(int arr[][cols],int rows,int rowSum[]){

    for(int i=1;i<=rows;i++){

        int sum=0;

        for(int j=1;j<=cols;j++){


            sum+= arr[i][j];
        }

        rowSum[i] = sum;
    }




}


void main(){

    int rows;

    printf("Enter Rows And Columns:");
    scanf("%d %d",&rows,&cols);

    if(rows>0 && cols>0){

        int arr[rows][cols],rowSum[rows];

        printf("Enter Elements:\n");

        for(int i=1;i<=rows;i++){

            for(int j=1;j<=cols;j++){

                scanf("%d",&arr[i][j]);
            }
        }

        rowsSum(arr,rows,rowSum);

        for(int i=1;i<=rows;i++){

            printf("%d ",rowSum[i]);
        }

        printf("\n");

    }

 
}