#include<stdio.h>
#include<stdbool.h>

int cols;

bool isMatrixEqual(int arr1[][cols],int arr2[][cols]){


    for(int i=1;i<=cols;i++){

        for(int j=1;j<=cols;j++){

            if(arr1[i][j] != arr2[i][j])
                return false;

        }
        
    }

    return true;
}


void main(){

    int rows;

    printf("Enter Size of Array1:");
    scanf("%d",&cols);

    printf("Enter Size of Array2:");
    scanf("%d",&rows);

    if(cols>0 && cols==rows){

        int arr1[cols][cols], arr2[cols][cols],finalarr[cols][cols];

        printf("Enter Elements of Array1:\n");

        for(int i=1;i<=cols;i++){

            for(int j=1;j<=cols;j++){

                scanf("%d",&arr1[i][j]);
            }
        }

        printf("Enter Elements of Array2:\n");

         for(int i=1;i<=cols;i++){

            for(int j=1;j<=cols;j++){

                scanf("%d",&arr2[i][j]);
            }
        }

        bool ret = isMatrixEqual(arr1,arr2);

         if(ret)
            printf("Matrics are Equal!\n");
         else
            printf("Matrics are not Equal!\n");

    }

 
}