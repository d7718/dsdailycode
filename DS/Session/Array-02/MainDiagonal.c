/*

    Main Diagonal Sum:
*/

#include<stdio.h>

int cols=0;

int mainDiagonalSum(int arr[][cols]){

    int sum=0;
  
    for(int i=1;i<=cols;i++){

        for(int j=1;j<=cols;j++){

            if(i==j)
                sum+= arr[i][j];
        }

    }

    return sum;




}


void main(){

    printf("Enter Size:");
    scanf("%d",&cols);

    if(cols>0){

        int arr[cols][cols];

        printf("Enter Elements:\n");

        for(int i=1;i<=cols;i++){

            for(int j=1;j<=cols;j++){

                scanf("%d",&arr[i][j]);
            }
        }

        int ret = mainDiagonalSum(arr);

        printf("Main Diagonal Sum:%d\n",ret);

        

    }

 
}