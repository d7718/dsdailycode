/*

    Column Sum:

*/


#include<stdio.h>

int cols=0;

int columnSum(int arr[][cols],int rows,int colSum[]){

    for(int i=1;i<=cols;i++){

        int sum=0;

        for(int j=1;j<=rows;j++){


            sum+= arr[j][i];
        }

        colSum[i] = sum;
    }




}


void main(){

    int rows;

    printf("Enter Rows And Columns:");
    scanf("%d %d",&rows,&cols);

    if(rows>0 && cols>0){

        int arr[rows][cols],colSum[cols];

        printf("Enter Elements:\n");

        for(int i=1;i<=rows;i++){

            for(int j=1;j<=cols;j++){

                scanf("%d",&arr[i][j]);
            }
        }

        columnSum(arr,rows,colSum);

        for(int i=1;i<=cols;i++){

            printf("%d ",colSum[i]);
        }

        printf("\n");

    }

 
}