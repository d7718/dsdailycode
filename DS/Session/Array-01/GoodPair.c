/*

  Good Pair:

    Problem Description:
        -Given an array A and an Integer B.
        -A pair(i,j) in the array is good pair if i!=j and (A[i]+A[j]==B)
        -Check if any good pair exist or not
        -Return 1 if good pair exist otherwise return 0

    Problem Constraints:
        1<= A.size()<=104
        1<=A[i]<=109
        1<=B<=109

*/


#include<stdio.h>

int goodPair_Approach1(int arr[],int len,int target){


    for(int i=0;i<len-1;i++){

        for(int j=i+1;j<len;j++){

            if(arr[i]+arr[j]==target)
                return 1;
        }
    }

    return 0;
}

int goodPair_Approach2(int arr[],int len,int target){

    return 0;
}

void main(){

    int len;

    printf("Enter Size:\n");
    scanf("%d",&len);

    if(len>0){

        int arr[len],target;

        printf("Enter Element:\n");

        for(int i=0;i<len;i++){

            scanf("%d",&arr[i]);
        }


        printf("Enter Target:\n");
        scanf("%d",&target);

        int ret = goodPair_Approach1(arr,len,target);

        if(ret)
           printf("Good Pair is Exist!\n");
        else
           printf("Good Pair Not Exist!\n");

    }

}
       