/*

  Count of elements :
     
    Problem Description:
        -Given an array A of N integers.
        -Count the number of elements that have at least 1 element greater than itself.

    Problem Contraints:
        1<=N<=105;
        1<=A[i]<=109

*/

#include<stdio.h>

int countofElement_Approach1(int arr[],int len){

    int cnt=0;

    for(int i=0;i<len;i++){

        for(int j=0;j<len;j++){

            if(arr[i]<arr[j]){

                cnt++;
                break;
            }
        }
    }

    return cnt;
}

int countofElement_Approach2(int arr[],int len){

    int max = arr[0],cnt=0;

    for(int i=0;i<len;i++){

        if(arr[i]>max){

            max = arr[i];
        }
    }

    for(int i=0;i<len;i++){

        if(arr[i]== max){

            cnt++;
        }
    }

    return len-cnt;
}

int main(){

    int len;

    printf("Enter Array Length:\n");
    scanf("%d",&len);


    if(len>0){


        int arr[] = {5,5,2};

        int ret;

        ret = countofElement_Approach1(arr,len);
         printf("Approach_1 :Count of Element:%d\n",ret);

         ret = countofElement_Approach2(arr,len);
         printf("Approach_2:Count of Element:%d\n",ret);
    }
}