/*
    Reverse in range:

        Problem Description:
            -Given  an array A of N integers.
            -Also Given are two integers B and C
            -Reverse the array A in the given range[B,C]
            -Return the array A after reversing in the given range.

        Problem Contraints:
            1<=N<=105
            1<=A[i]<=109
            0<=B<=C<=N-1
*/

#include<stdio.h>

int reverseInRange(int arr[],int len,int start,int end){


    if(start>end || end>len-1){
        return -1;
    }

    int j=0,temp;

    for(int i=start;i<=(start+end)/2;i++){

        temp = arr[i];
        arr[i] = arr[end-j];
        arr[end-j] = temp;
        j++;

    }

    return 0;
}

void main(){

    int len;

    printf("Enter Size:\n");
    scanf("%d",&len);

    if(len>0){

        int arr[len],start,end;

        printf("Enter Element:\n");

        for(int i=0;i<len;i++){

            scanf("%d",&arr[i]);
        }

        printf("Enter Start And End:\n");
        scanf("%d %d",&start,&end);

        if(start>=0 && end>0){

            printf("In main if\n");

            reverseInRange(arr,len,start,end);
       
            for(int i=0;i<len;i++){

                printf("%d ",arr[i]);
            }

            printf("\n");
        }
    }
}