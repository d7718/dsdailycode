/*
    Reverse the Array:

        Problem Description:
            -Given an constant  array A
            -Required to return another array which is the reversed form of input array
            -Return an integer array

        Problem Contraints:
            1<=A.size()<=10000
            1<= A[i] <=10000
*/

#include<stdio.h>

int reverseArray(int arr[],int len){

    int temp;

    for(int i=0;i<len/2;i++){

        temp = arr[i];
        arr[i] = arr[len-1-i];
        arr[len-1-i] = temp;
    }

    return 0;
}

void main(){

    int len,key;

    printf("Enter Size:\n");
    scanf("%d",&len);

    if(len>0){

        int arr[len],start,end;

        printf("Enter Element:\n");

        for(int i=0;i<len;i++){

            scanf("%d",&arr[i]);
        }

        reverseArray(arr,len);
       
        for(int i=0;i<len;i++){

            printf("%d ",arr[i]);
        }

            printf("\n");
    }
}