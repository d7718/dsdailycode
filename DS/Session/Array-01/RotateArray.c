/*
    Array Rotation:

    Problem Description:
        - Given an integer array A of size N and an integer B,you have to return the same array
          after rotating it B times towards the right.
        - Return the array A after rotating it B times to the right.

    Problem Contraints:
        1<=N<=105
        1<=A[j]<=109
        1<=B<=109



*/



#include<stdio.h>

int rotateArray(int arr[],int len,int rotation){

    while(rotation){

        int j=1,end=arr[len-1];

        for(int i=len-1;i>=1;i--){

            arr[i] = arr[len-1-j];
            j++;
        }

         arr[0] = end;
         rotation--;
    }
}


void main(){

    int len,key;

    printf("Enter Size:\n");
    scanf("%d",&len);

    if(len>0){

        int arr[len],rotation;

        printf("Enter Element:\n");

        for(int i=0;i<len;i++){

            scanf("%d",&arr[i]);
        }

        printf("Enter Rotation:\n");
        scanf("%d",&rotation);

        rotateArray(arr,len,rotation);
       
        for(int i=0;i<len;i++){

            printf("%d ",arr[i]);
        }

        printf("\n");

        

    }
}