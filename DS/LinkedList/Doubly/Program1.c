
#include<stdio.h>
#include<stdlib.h>

struct Demo{
        
	struct Demo *prev;
	int data;
	struct Demo *next;

};


struct Demo *head = NULL;

//createNode

struct Demo *createNode(){

      struct Demo *newNode = (struct Demo*)malloc(sizeof(struct Demo));

      printf("Enter Data:\n");
      scanf("%d",&(newNode->data));

      newNode->prev = NULL;
      newNode->next = NULL;

      return newNode;
}


//count

int count(){

	struct Demo *temp = head;
	int count = 0;

	while(temp!=NULL){

		count++;
		temp = temp->next;
	}

	return count;
}



//addNode
void addNode(){

	struct Demo *newNode = createNode();

	if(head==NULL){

	   head = newNode;

	}else{

             struct Demo *temp = head;

	     while(temp->next != NULL){

	            temp = temp->next;

              }

	      temp->next = newNode;
	      newNode->prev = temp;

	}

}


//addFirst

void addFirst(){

	struct Demo *newNode =createNode();

	if(head==NULL){
		
		head = newNode;
	}else{
		newNode->next = head;
		head->prev = newNode;
		head =  newNode;
	}
}

//addAtPos

void addAtPos(int pos){

	int cnt = count();

	if(pos<=0 || pos>=cnt+2){

		printf("Invalid Position\n");
	
	}else{
		if(pos==1)
			addFirst();
	        
		else if(pos==cnt+1)
			addNode();
		else{

			struct Demo *newNode = createNode();
			struct Demo *temp = head;

			while(pos-2){

				temp = temp->next;
				pos--;
			}

			newNode->next =  temp->next;
			newNode->prev =  temp;
			temp->next->prev = newNode;
			temp->next = newNode;
	       }
	}
}

//printLL

void printLL(){

	if(head==NULL){

		printf("LinkedList is Empty!\n");
	}else{

                struct Demo *temp = head;

	        while(temp!=NULL){

		    if(temp->next==NULL){	 
	        
			printf("|%d|",temp->data);
		   
		    } else{
	        	
			printf("|%d|-->",temp->data);
		   }
		
		    temp = temp->next;
	     }

	}
	printf("\n");
}


//deleteFirst

void deleteFirst(){


	if(head==NULL){

		printf("LinkedList is EMPTY !\n");
	
	}else{

		head = head->next;
		free(head->prev);
		head->prev = NULL;
	}
}


//deleteLast

void deleteLast(){


	if(head==NULL){
		
		printf("LinkedList is EMPTY !\n");
	
	}else{ 
		
	     struct Demo *temp = head;
	     	
	     	if(head->next==NULL){

			free(temp);
			head=NULL;
		
		}else{

	                while(temp->next->next !=NULL){

		                  temp = temp->next;
                	}

			free(temp->next);
			temp->next = NULL;

	     }
       }
}


void deleteAtPos(int pos){


	int cnt = count();

	if(pos<=0 || pos>cnt){

		printf("Invalid Position!\n");
	
	}else{
		if(pos==1)

			deleteFirst();

		else if(pos==cnt)

			deleteLast();

		else{
			struct Demo * temp = head;


			while(pos-2){

				temp = temp->next;
				pos--;
			}

			temp->next = temp->next->next;
			free(temp->next->prev);
			temp2->next->prev  = temp;
		
		}
	}



}

void main(){

        char choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.deleteFirst\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtPos\n");
		printf("7.printLL\n");

		int ch;
		printf("Enter Choice:\n");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;

			case 3:
				{
			          int pos;
				  printf("Enter position:\n");
				  scanf("%d",&pos);

				  addAtPos(pos);
				}
				  break;

		       case 4:
				  deleteFirst();
				  break;

		       case 5:
				  deleteLast();
				  break;

		       case 6:
		              {
				  int pos;
				  printf("Enter position:\n");
				  scanf("%d",&pos);
				  
				  deleteAtPos(pos);
			      }
				  break;
		       
		       case 7:
				  printLL();
				  break;

	               default:
				  printf("Invalid Choice!\n");

	    }
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	
     }while(choice=='y'||choice=='Y');

}

	   
