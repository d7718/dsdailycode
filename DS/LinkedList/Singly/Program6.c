

#include<stdio.h>
#include<stdlib.h>


typedef struct Employee{

	char empName[20];
	int empId;
	struct Employee *next;

}Emp;


Emp *head = NULL;

//createNode

Emp *createNode(){

      Emp *newNode = (Emp*)malloc(sizeof(Emp));

      getchar();
 
      printf("Enter Employee  Name:\n");
      int i=0;
      char ch;

      while((ch=getchar())!='\n'){

	      (*newNode).empName[i++] = ch;
      }

      printf("Enter Employee Id:\n");
      scanf("%d",&(newNode->empId));

      newNode->next = NULL;

      return newNode;
}

//addNode
void addNode(){

	Emp *newNode = createNode();

	if(head==NULL){

	   head = newNode;

	}else{

             Emp *temp = head;

	     while(temp->next != NULL){

	            temp = temp->next;

              }

	      temp->next = newNode;

	}

}


//addFirst

void addFirst(){

	Emp *newNode =createNode();

	if(head==NULL){
		
		head = newNode;
	}else{
		newNode->next = head;
		head =  newNode;
	}
}

//addAtPos

void addAtPos(int pos){

	Emp *newNode = createNode();
	Emp *temp = head;

	while(pos-2){

		temp = temp->next;
		pos--;
	}

	newNode->next =  temp->next;
	temp->next = newNode;
}

//printLL

void printLL(){

	if(head==NULL){

		printf("LinkedList is Empty!\n");
	}else{

                Emp *temp = head;

	        while(temp!=NULL){

		    if(temp->next==NULL){	 
	        
		    	printf("|%s",temp->empName);
			printf("|%d|",temp->empId);
		   
		    } else{
	        	
		    	printf("|%s",temp->empName);
			printf("|%d|-->",temp->empId);
		   }
		
		    temp = temp->next;
	     }

	}
	printf("\n");
}

void main(){

	int count;

	printf("Enter Total Node:\n");
	scanf("%d",&count);

	for(int i =0;i<count;i++){

		addNode();
	}

	addFirst();

	printLL();

}

	   
