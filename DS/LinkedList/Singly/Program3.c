// Head is defined Local

#include<stdio.h>
#include<stdlib.h>

typedef struct Student{

	int id;
	float ht;
	struct Student *next;

}stud;


void addNode(stud *head){

	stud *newNode = (stud*)malloc(sizeof(stud));

	newNode->id = 1;
	newNode->ht = 5.2f;
	head = newNode;

}


void printNode(stud *head){

	stud *temp = head;

	while(temp!=NULL){

		printf("ID=%d ",temp->id);
		printf("height=%f ",temp->ht);

		temp=temp->next;
	}
}

void main(){

	stud *head = NULL;

	addNode(head);
	printNode(head);

}
