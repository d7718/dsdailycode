

#include<stdio.h>
#include<stdlib.h>

struct Demo{

	int data;
	struct Demo *next;

};


struct Demo *head = NULL;

//createNode

struct Demo *createNode(){

      struct Demo *newNode = (struct Demo*)malloc(sizeof(struct Demo));

      printf("Enter Data:\n");
      scanf("%d",&(newNode->data));

      newNode->next = NULL;

      return newNode;
}


//count

int count(){

	struct Demo *temp = head;
	int count = 0;

	while(temp!=NULL){

		count++;
		temp = temp->next;
	}

	return count;
}



//addNode
void addNode(){

	struct Demo *newNode = createNode();

	if(head==NULL){

	   head = newNode;

	}else{

             struct Demo *temp = head;

	     while(temp->next != NULL){

	            temp = temp->next;

              }

	      temp->next = newNode;

	}

}

//printLL

void printLL(){

	if(head==NULL){

		printf("LinkedList is Empty!\n");
	}else{

                struct Demo *temp = head;

	        while(temp!=NULL){

		    if(temp->next==NULL){	 
	        
			printf("|%d|",temp->data);
		   
		    } else{
	        	
			printf("|%d|-->",temp->data);
		   }
		
		    temp = temp->next;
	     }

	}
	printf("\n");
}


void search(int num){

	struct Demo *temp =  head;

	int cnt=0,index1=0,index2=0;

	while(temp!=NULL){

		cnt++;

		if(temp->data==num){

			index2 = index1;
			index1 = cnt;

		}

		temp = temp->next;
	}

	if(index1==0){

		printf("%d is not Present in LinkedList!\n",num);
	
	}else{
		if(index2){

			printf("%d is Present and its second Last Occurence is %d\n",num,index2);
		}else{
			printf("%d is Present Only Once at Index %d\n",num,index1);
		}
	}
}


			
void main(){

	int n;

    
	printf("Enter The total node:\n");
	scanf("%d",&n);

        if(n>=2){

		for(int i=0;i<n;i++){

			addNode();
		}

		printLL();

		int num;

		printf("Enter number to be searched in LinkedList:\n");
        	scanf("%d",&num);

       		 search(num);	

	}else{
		printf("Minimum two nodes required!\n");
        }

}

	   
