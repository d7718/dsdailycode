//User Input Singly Linked List

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{

	char mName[20];
	float imdb;
	struct Movie *next;

}Mov;


Mov *head = NULL;

void addNode(){

	Mov *newNode = (Mov*)malloc(sizeof(Mov));

	printf("Enter the Movie Name:\n");
	fgets(newNode->mName,15,stdin);

	printf("Enter the Rating:\n");
	scanf("%f",&(newNode->imdb));

	getchar();

	newNode->next = NULL;

	if(head==NULL){

	   head = newNode;

	}else{

             Mov *temp = head;

	     while(temp->next != NULL){

	            temp = temp->next;

              }

	      temp->next = newNode;

	}

}


void printLL(){

	Mov *temp = head;

	while(temp!=NULL){

		char *str = temp->mName;

		printf("|");
	         
	        while(*str!='\n'){

			printf("%c",*str);
		        str++;			
		}

                if(temp->next==NULL)	 
	        	printf("|%0.2f|",temp->imdb);
                else
	        	printf("|%0.2f|->",temp->imdb);
		temp = temp->next;
		

	}
}

void main(){

	addNode();
        addNode();
        addNode();

        printLL();

	printf("\n");

}

	   
