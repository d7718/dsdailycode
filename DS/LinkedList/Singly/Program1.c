#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

void main(){

	struct Node *newNode =(struct Node*)malloc(sizeof(struct Node));
	struct Node *head = NULL;

        // First Node
	newNode->data = 10;
	newNode->next = NULL;

	//Connecting First Node....
	head = newNode;


	//Second Node
	newNode =(struct Node*)malloc(sizeof(struct Node));
	newNode->data = 20;
	newNode->next = NULL;

	//Connecting Second Node....
	head->next = newNode;
	
	//Third Node
	newNode =(struct Node*)malloc(sizeof(struct Node));
	newNode->data = 30;
	newNode->next = NULL;
	
	//Connecting Third Node....
	head->next->next = newNode;

	//Printing LinkedList
	
	struct Node *temp = head;

	while(temp!=NULL){

	    printf("%d ",temp->data);
	    temp = temp->next;
	}

}
	
