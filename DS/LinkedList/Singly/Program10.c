/*
   WAP a real-time example for Singly LinkedList.And Perform LinkedList Operations on it

*/

#include<stdio.h>
#include<stdlib.h>

//Self-referencial Structure Size:40 byte

typedef struct Artist{
	
	char aName[20];
	int nSong;
	float cFee;
	struct Artist *next;

}Art;

Art *head = NULL;

//Creating newNode

Art* createNode(){

   Art *newNode = (Art*)malloc(sizeof(Art));

   getchar();
 
   printf("Enter the Artist Name:\n");
   char ch;
   int i=0;
  
  while((ch=getchar())!='\n'){
	  
	  (*newNode).aName[i++] = ch;
  }
  
  printf("Enter the No of Songs:\n");
  scanf("%d",&(newNode->nSong));
  
  printf("Enter the Consert Fees:\n");
  scanf("%f",&(newNode->cFee));
  
  newNode->next = NULL;
 
   return newNode;
 
} 
  

//Print LinkedList

void printLL(){

	if(head==NULL){

		printf("LinkedList is EMPTY!\n");
	}else{

                Art *temp = head;
	
		while(temp!=NULL){
		
			printf("|%s",temp->aName);
			printf("|%d",temp->nSong);
		
	     		if(temp->next!=NULL)
			
		      		printf("|%0.2f|-->",temp->cFee);
               	        else
                      		printf("|%0.2f|",temp->cFee);
              
        		temp =temp->next;
		}
    
                  printf("\n");
        }
}

//Node Count

int count(){

	Art *temp = head;
	int count = 0;

	while(temp!=NULL){

		count++;
		temp = temp->next;
	}

	return count;
}


//addNode OR addLast

void addNode(){

	Art *newNode = createNode();

        if(head==NULL){

	   head = newNode;

	}else{

             Art *temp = head;

	     while(temp->next != NULL){

	            temp = temp->next;

              }

	      temp->next = newNode;

	}

}


//addFirst

void addFirst(){

	Art *newNode =createNode();

	if(head==NULL){
		
		head = newNode;
	}else{
		newNode->next = head;
		head =  newNode;
	}
}

//addAtPos

void addAtPos(int pos){

	int cnt = count();

	if(pos<=0 || pos>=cnt+2){

		printf("Invalid Position\n");
	
	}else{
		if(pos==1)
			addFirst();
	        
		else if(pos==cnt+1)
			addNode();
		
		else{

			Art *newNode = createNode();
			Art *temp = head;

			while(pos-2){

				temp = temp->next;
				pos--;
			}

			newNode->next =  temp->next;
			temp->next = newNode;
	       }
	}
}

//deleteFirst

void deleteFirst(){


	if(head==NULL){

		printf("LinkedList is EMPTY !\n");
	
	}else{

		Art *temp = head;
		head = temp->next;
		free(temp);
	}
}


//deleteLast

void deleteLast(){


	if(head==NULL){
		
		printf("LinkedList is EMPTY !\n");
	
	}else{ 
		
	     Art *temp = head;
	     	
	     	if(head->next==NULL){

			     free(temp);
			     head=NULL;
		
		}else{

	                while(temp->next->next !=NULL){

		                  temp = temp->next;
                	}

			free(temp->next);
			temp->next = NULL;

	     }
       }
}


// deleteAtPos

void deleteAtPos(int pos){


	int cnt = count();

	if(pos<=0 || pos>cnt){

		printf("Invalid Position!\n");
	
	}else{
		if(pos==1)

			deleteFirst();

		else if(pos==cnt)

			deleteLast();

		else{
			Art * temp1 = head;


			while(pos-2){

				temp1 = temp1->next;
				pos--;
			}

			Art *temp2 = temp1->next;
			temp1->next = temp2->next;
			free(temp2);
		}
	}



}

void main(){

        char choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.deleteFirst\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtPos\n");
		printf("7.count Node\n");
		printf("8.printLL\n");

		int ch;
		printf("Enter Choice:\n");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;

			case 3:
				{
			      int pos;
				  printf("Enter position:\n");
				  scanf("%d",&pos);

				  addAtPos(pos);
				}
				  break;

		       case 4:
				  deleteFirst();
				  break;

		       case 5:
				  deleteLast();
				  break;

		       case 6:
		              {
				  int pos;
				  printf("Enter position:\n");
				  scanf("%d",&pos);
				  
				  deleteAtPos(pos);
			      }
				  break;
		       
		       case 7:
				  printf("Count:%d\n",count());
				  break;
		      
		       case 8:
				  printLL();
				  break;

	           default:
				  printf("Invalid Choice!\n");

	    }
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	
     }while(choice=='y'||choice=='Y');

}
