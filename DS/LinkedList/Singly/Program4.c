// Head is defined Global

#include<stdio.h>
#include<stdlib.h>

typedef struct Student{

	int id;
	float ht;
	struct Student *next;

}stud;


stud *head = NULL;


void addNode(){

	stud *newNode = (stud*)malloc(sizeof(stud));

	newNode->id = 1;
	newNode->ht = 5.2f;
	head = newNode;

}


void printNode(){

	stud *temp = head;

	while(temp!=NULL){

		printf("|%d ",temp->id);
		printf("|%0.2f|",temp->ht);

		temp=temp->next;
	}
	printf("\n");
}

void main(){


	addNode();
	printNode();

}
