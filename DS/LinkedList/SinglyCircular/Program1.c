

#include<stdio.h>
#include<stdlib.h>

struct Demo{

	int data;
	struct Demo *next;

};


struct Demo *head = NULL;

//createNode

struct Demo *createNode(){

      struct Demo *newNode = (struct Demo*)malloc(sizeof(struct Demo));

      printf("Enter Data:\n");
      scanf("%d",&(newNode->data));

      newNode->next = NULL;

      return newNode;
}


//count

int count(){

	struct Demo *temp = head;
	int count = 0;

	if(head==NULL){

		return count;
	}else{

	while(temp->next!=head){

		count++;
		temp = temp->next;
	}

	printf("%d\n",count);

	return count+1;
     }
}



//addNode
void addNode(){

	struct Demo *newNode = createNode();

	if(head==NULL){

	   head = newNode;
	   newNode->next = head;

	}else{

             struct Demo *temp = head;

	     while(temp->next != head){

	            temp = temp->next;

              }

	      temp->next = newNode;
	      newNode->next = head;

	}

}


//addFirst

void addFirst(){

	struct Demo *newNode =createNode();

	if(head==NULL){
		
		head = newNode;
		newNode->next = head;
	}else{

		struct Demo *temp = head;

		while(temp->next != head){

			temp = temp->next;
		}
		temp->next = newNode;
		newNode->next = head;
		head =  newNode;
	}
}

//addAtPos

void addAtPos(int pos){

	int cnt = count();

	if(pos<=0 || pos>=cnt+2){

		printf("Wrong Position\n");
	
	}else{
		if(pos==1)
			addFirst();
	        
		else if(pos==cnt+1)
			addNode();
		else{

			struct Demo *newNode = createNode();
			struct Demo *temp = head;

			while(pos-2){

				temp = temp->next;
				pos--;
			}

			newNode->next =  temp->next;
			temp->next = newNode;
	       }
	}
}

//printLL

void printLL(){

	if(head==NULL){

		printf("LinkedList is Empty!\n");
	}else{

                struct Demo *temp = head;

	        while(temp->next != head){

                        printf("|%d|-->",temp->data);
		        temp = temp->next;
		   
		} 
	        	
	        printf("|%d|",temp->data);
		  
	}
	
	printf("\n");
}


//deleteFirst

void deleteFirst(){


	if(head==NULL){

		printf("LinkedList is EMPTY !\n");
	
	}else{
	
                struct Demo *temp = head;

		if(temp->next == head){

			free(temp);
			head = NULL;
		}else{

			while(temp->next!=head){

				temp = temp->next;
			}

                  head = head->next;
		         free(temp->next);
			 temp->next = head;
		}
	}
}


//deleteLast

void deleteLast(){


	if(head==NULL){
		
		printf("LinkedList is EMPTY !\n");
	
	}else{ 
		
	     struct Demo *temp = head;
	     	
	     	if(head->next==head){

			free(temp);
			head=NULL;
		
		}else{

	                while(temp->next->next !=head){

		                  temp = temp->next;
                	}

			free(temp->next);
			temp->next = head;

	     }
       }
}


void deleteAtPos(int pos){


	int cnt = count();

	if(pos<=0 || pos>cnt){

		printf("Invalid Position!\n");
	
	}else{
		if(pos==1)

			deleteFirst();

		else if(pos==cnt)

			deleteLast();

		else{
			struct Demo * temp1 = head;


			while(pos-2){

				temp1 = temp1->next;
				pos--;
			}

			struct Demo *temp2 = temp1->next;
			temp1->next = temp2->next;
			free(temp2);
		}
	}



}

void main(){

        char choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.deleteFirst\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtPos\n");
		printf("7.printLL\n");

		int ch;
		printf("Enter Choice:\n");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;

			case 3:
				{
			          int pos;
				  printf("Enter position:\n");
				  scanf("%d",&pos);
                                  
				  addAtPos(pos);
				}
				  break;

		       case 4:
				  deleteFirst();
				  break;

		       case 5:
				  deleteLast();
				  break;

		       case 6:
		              {
				  int pos;
				  printf("Enter position:\n");
				  scanf("%d",&pos);
				  
				  deleteAtPos(pos);
			      }
				  break;
		       
		       case 7:
				  printLL();
				  break;

	               default:
				  printf("Invalid Choice!\n");

	    }
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	
     }while(choice=='y'||choice=='Y');

}

	   
