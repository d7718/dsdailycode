#include<stdio.h>

int linearSearch(int arr[],int len,int key){

    for(int i=0;i<len;i++){

        if(key== arr[i])
           return i;
    }

    return -1;

}


void main(){

    int len,key;

    printf("Enter Size:\n");
    scanf("%d",&len);

    if(len>0){

        int arr[len];

        printf("Enter Element:\n");

        for(int i=0;i<len;i++){

            scanf("%d",&arr[i]);
        }

        printf("Enter Key:\n");
        scanf("%d",&key);

        int ret = linearSearch(arr,len,key);

        if(ret==-1)
            printf("Key Not Found!\n");
        else
            printf("Key Found At Index:%d\n",ret);
   
    }else{

        printf("Invalid Array Size!\n");
    }

}