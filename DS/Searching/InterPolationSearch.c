/*
   Sorted Array with Uniform difference

   Index = start+(end-start)* ((key-arr[start])/(arr[end]-arr[start]))
*/


#include<stdio.h>

int interpolationSearch(int arr[],int end,int key){

    int start =0;

    int index =  start+((end-start)*((key-arr[start])/(arr[end]-arr[start])));

    for(int i=0;i<=end;i++){

        if(arr[index]==key)
            return index;

        if(arr[index]<key)
             index++;
        else
             index--;

    }

    return -1;
}


void main(){

    int len,key;

    int arr[] = {3,9,12,15,18};

    printf("Enter Key:\n");
    scanf("%d",&key);

    int ret = interpolationSearch(arr,4,key);

    if(ret==-1)
         printf("Key Not Found!\n");
    else
        printf("Key Found At Index:%d\n",ret);
}