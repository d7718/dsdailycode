#include<stdio.h>

int firstOccurrence(int arr[],int end,int key){

    int start=0,index=-1;

    while(start<=end){

        int mid = (start+end)/2;

        if(arr[mid]==key){

            if(arr[mid-1]<key)
               return mid;

            index = mid;
        }

        if(arr[mid]<key)
           start = mid+1;
        else
           end = mid-1;
    }

    return index;
}

void main(){

    int len,key;

    int arr[] = {2,3,4,5,5,5,7,7,8,9,9,9,11,12};

    printf("Enter Key:\n");
    scanf("%d",&key);

    int ret = firstOccurrence(arr,13,key);

    if(ret==-1)
         printf("Key Not Found!\n");
    else
        printf("FirstOccurrence of Key at Index:%d\n",ret);
}