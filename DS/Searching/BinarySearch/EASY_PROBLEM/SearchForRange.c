#include<stdio.h>

int startI=-1,endI=-1;

void searchForRange(int arr[],int end,int key){

    int start =0;

    while(start<=end){

        int mid = (start+end)/2;

        if(arr[mid]==key){

            if(arr[mid-1]!=key || mid==0){

                startI=mid;
                endI = mid;
            
            }else if(startI==-1||startI>mid){

                startI = mid-1;
                endI = mid;
            }else{

                endI = mid;
            }
        }

        if(arr[mid]<= key){

            start = mid+1;
        }else{

            end = mid-1;
        }
    }
}

void main(){

    int len,key;

    int arr[] = {5,7,7,8,8,8};

    printf("Enter Key:\n");
    scanf("%d",&key);

    searchForRange(arr,5,key);

    printf("Start: %d\n",startI);
    printf("End: %d\n",endI);
}