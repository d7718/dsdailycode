#include<stdio.h>

int binarySearch(int arr[],int end,int key){

    int start =0;

    while(start<=end){

        int mid = (start+end)/2;

        if(arr[mid]==key)
            return mid;

        if(arr[mid]<key)
            start = mid+1;
        else
           end = mid-1;
    }

    return -1;

    
}

void main(){

    int len,key;

    int arr[] = {3,7,11,13,14,17,19,25,35,50};

    printf("Enter Key:\n");
    scanf("%d",&key);

    int ret = binarySearch(arr,9,key);

    if(ret==-1)
         printf("Key Not Found!\n");
    else
        printf("Key Found At Index:%d\n",ret);
}