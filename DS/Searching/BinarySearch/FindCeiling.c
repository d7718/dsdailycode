/*

   int arr[] = {2,4,5,7,11,17,21}

   key =9
   
   floor =7

*/

#include<stdio.h>

int findceiling(int arr[],int end,int key){

    int start=0,ceil;

    if(arr[end]<key)
        return -1;

    while(start<=end){

        int mid = (start+end)/2;

        if(arr[mid]==key)
           return arr[mid];
        
        if(arr[mid]<key){

            start = mid+1;
        
        }else{

            end = mid-1;
            ceil = arr[mid];
        }

    }

    return ceil;
}


void main(){

    int len,key;

    int arr[] = {2,4,5,7,11,17,21};

    printf("Enter Key:\n");
    scanf("%d",&key);

    int ret = findceiling(arr,6,key);

    if(ret==-1)
         printf("Ceiling Not Found!\n");
    else
        printf("Ceiling:%d\n",ret);

}