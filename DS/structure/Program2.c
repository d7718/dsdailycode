#include<stdio.h>


void main(){

	char arr[] = {'A','B','C','D','E'};

	char *cptr = arr;

	int  *iptr = arr; // warning: initialization of ‘int *’ from incompatible pointer type ‘char *’ [-Wincompatible-pointer-types]
		

	printf("%c\n",*cptr);
	printf("%c\n",*iptr);

	cptr++;
	iptr++;
	
	printf("%c\n",*cptr);
	printf("%c\n",*iptr);

}





