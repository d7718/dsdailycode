//void * malloc(size_t)


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct OTT {

	char pName[10];
	int user;
	float price;
};

void main(){

	struct OTT *ptr1 = (struct OTT*) malloc(sizeof(struct OTT));

	strcpy(ptr1->pName,"PrimeVideo");
	ptr1->user = 50;
	ptr1->price = 450.35;

	printf("Platform: %s\n",ptr1->pName);
	printf("User: %d\n",ptr1->user);
	printf("Price: %f\n",ptr1->price);

}
