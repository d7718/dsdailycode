
// Structure Pointer

#include<stdio.h>

struct Movie{

	char mName[10];
	int count;
	float rating;

}obj1 = {"SitaRaman",2,8.5};


void main(){

	typedef struct Movie mv;

	mv obj2 = {"Kartikey2",3,8.1};

	mv *ptr1 = &obj1;
	struct Movie *ptr2 = &obj2;

	printf("Movie: %s\n",(*ptr1).mName);
	printf("Ticket: %d\n",(*ptr1).count);
	printf("Rating: %f\n",(*ptr1).rating);
	
	printf("\n");

	printf("Movie: %s\n",ptr2->mName);
	printf("Ticket: %d\n",ptr2->count);
	printf("Rating: %f\n",ptr2->rating);

}


