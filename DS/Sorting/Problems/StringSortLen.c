#include<stdio.h>


int mystrlen(char *str){

    int cnt=0;

    while(*str != '\0'){

        cnt++;
        str++;
    }

    return cnt;
}

void swap(char** str1,char**str2){

    char *temp = *str1;
    *str1 = *str2;
    *str2 = temp;


}

int strCompare(char *str1, char *str2){

    while(*str1 != '\0'){

        if(*str1 != *str2)
           return *str1 - *str2;

        str1++,str2++;
    }

    return 0;
}

void stringSortLen(char* arr[],int len){

    for(int i=0;i<=len-1;i++){

        for(int j=0;j<len-1-i;j++){

            if(mystrlen(arr[j])>mystrlen(arr[j+1])){

                 swap(&arr[j],&arr[j+1]);

            }

            if(mystrlen(arr[j])== mystrlen(arr[j+1])){ 

                 if(strCompare(arr[j],arr[j+1])>0){

                    swap(&arr[j],&arr[j+1]);


                }

             }
        }
    }


}

void main(){

    char *arr[] = {"7", "Kahna","Ashish","AK","Shashi","Datta","Akshay","Raj","9","Ashaky"};

    stringSortLen(arr,10);


    for(int i=0;i<10;i++){

        printf("%s ",arr[i]);
    }

    printf("\n");


}