#include<stdio.h>
#include<stdbool.h>

bool isSmaller(char*,char*);

int mystrlen(char *str){

    int cnt=0;

    while(*str != '\0'){

        cnt++;
        str++;
    }

    return cnt;
}

int strCompare(char *str1, char *str2){

    while(*str1 != '\0'){

        if(*str1 != *str2)
           return *str1 - *str2;

        str1++,str2++;
    }

    return 0;
}

void stringSortChar(char* arr[],int len){

    for(int i=1;i<len;i++){

        char *target = arr[i];

        int j=i-1;

        int ret = strCompare(arr[j],target);

       // printf("%d\n",ret);

        for(;j>=0 ;j--){

            if(strCompare(arr[j],target)>0){

                arr[j+1] =arr[j];
           
            }else{

                break;
            }
        }

         arr[j+1] = target;
        // printf("%s\n",arr[j+1]);
    }
}




void main(){

    char *arr[] = {"Aaj","Rahul","Ajay","A","BB","Ashish","Datta"};

    stringSortChar(arr,7);


    for(int i=0;i<7;i++){

        printf("%s ",arr[i]);
    }

    printf("\n");


}