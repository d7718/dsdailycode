#include<stdio.h>

void bubbleSort(int arr[],int len){

    int i=0,j=0;

    for(i=0;i<=len-1;){

        if(j<len-1-i){

            if(arr[j]>arr[j+1]){

                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }

            j++;
            continue;
        }else{

            i++;
            j=0;
        }
    }
}


void main(){

    int arr[] = {-5,-6,3,5,2};

    bubbleSort(arr,5);

    for(int i=0;i<5;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");
}