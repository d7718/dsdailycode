#include<stdio.h>


void countingSort(int arr[],int len,int pos){

   int countArr[10]={0};


     // Count the occurrence of each element in array
    for(int i=0;i<len;i++){

        countArr[(arr[i]/pos)%10]++;
        
    }


    // Find cumulative sum of countArr
    
     for(int i=1;i<=10;i++){

        countArr[i] = countArr[i-1]+countArr[i];
    }


    // Create new array of size len

    int output[len];

    for(int i = len-1;i>=0;i--){

        output[countArr[arr[i]/pos]-1] = arr[i];
        countArr[(arr[i]/pos)%10]--;
    }

    // Copy element to original array and add min term to get original array

    for(int i=0;i<len;i++){

        arr[i] = output[i];
    }


}

void radixSort(int arr[],int N){

    //1. Find Maximum element

    int max= arr[0];

    for(int i=1;i<N;i++){

        if(max<arr[i])
            max= arr[i];


    }

    // 2.Count digit in maxmimum element

    for(int pos =1;max/pos>1;pos=pos*10){

        countingSort(arr,N,pos);
    }

}



    


void main(){

    int arr[] =  {-3,7,-2,1,8,2,5,2,7,-4};

    int len = sizeof(arr)/sizeof(int);

    printf("Before Sorting: ");

    for(int i=0;i<len;i++){

        printf("%d ",arr[i]);
    }

    radixSort(arr,len);

    printf("\nAfter Sorting ");

    for(int i=0;i<len;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");

}