#include<stdio.h>


void swap(int *a,int*b){

    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int arr[],int start,int end){

    int pivot =  arr[end];
    int index = start-1;

    for(int j=start;j<end;j++){

        if(arr[j]<pivot){

            index++;
            swap(&arr[j],&arr[index]);
        }
    }

    swap(&arr[index+1],&arr[end]);

    return index+1;
}


void quickSort(int arr[],int start,int end){

    if(start<end){

        int pivot = partition(arr,start,end);
        quickSort(arr,start,pivot-1);
        quickSort(arr,pivot+1,end);
    }
}


void main(){

     int arr[] = {3,4,2,7,33,45,9};

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    int start=0,end=6;
    quickSort(arr,start,end);

    printf("\nAfter QuickSort\n");

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");
}