#include<stdio.h>

int partition(int arr[],int start,int end){

    int temp[end-start+1];
    int index =0;

    int pivot = arr[end];

    for(int i=start;i<end;i++){

        if(arr[i]<=pivot)
            temp[index++]=arr[i];
    }

    int pos = index+start;
    temp[index++]=pivot;

    for(int i= start;i<end;i++){

        if(arr[i]>pivot)
           temp[index++] = arr[i];
    }

    for(int i=start;i<=end;i++){

        arr[i] = temp[i-start];
    }

    return pos;
}


void quickSort(int arr[],int start,int end){

    if(start<end){

        int pivot = partition(arr,start,end);
        quickSort(arr,start,pivot-1);
        quickSort(arr,pivot+1,end);
    }
}


void main(){

     int arr[] = {3,4,3,7,33,45,9};

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    int start=0,end=6;
    quickSort(arr,start,end);

    printf("\nAfter QuickSort\n");

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");
}