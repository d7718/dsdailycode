#include<stdio.h>


void merge(int arr[],int start,int mid,int end){

    int ele1 = mid-start+1;
    int ele2 = end-mid;

    int arr1[ele1],arr2[ele2];

    int i=0,j=0,k=start;

    for(;i<ele1;i++){

        arr1[i] = arr[start+i];
    }

    for(;j<ele2;j++){

        arr2[j] = arr[mid+1+j];
    }


    i=0;
    j=0;

    while(i<ele1 && j<ele2){

        if(arr1[i]<arr2[j]){

            arr[k] =  arr1[i];
            i++;
        }else{

            arr[k] = arr2[j];
            j++;
        }

        k++;
    }

    while(i<ele1){

        arr[k++] = arr1[i++];
    }

    while(j<ele2){

        arr[k++] = arr2[j++];
    }

   
}



void mergeSort(int arr[],int start,int end){

    if(start<end){

        int mid = (start+end)/2;

        mergeSort(arr,start,mid);
        mergeSort(arr,mid+1,end);
        merge(arr,start,mid,end);
    }

}

void main(){

    int arr[] = {3,4,2,7,33,45,9};

    for(int i=0;i<7;i++){

        printf("%d\n",arr[i]);
    }

    int start=0,end=6;
    mergeSort(arr,start,end);

    printf("After MergeSort\n");

    for(int i=0;i<7;i++){

        printf("%d\n",arr[i]);
    }
}
