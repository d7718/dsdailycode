#include<stdio.h>

void countingSort_NonNegative(int arr[],int len){

    int max = arr[0];

    // 1. Find max element
    for(int i=1;i<len;i++){

        if(max<arr[i])
            max = arr[i];
    }

     // 2. Define array of size max+1 and assign all elements to 0;
    int countArr[max+1];

    for(int i=0;i<=max;i++){

        countArr[i] = 0;
    }


     // 3. Count the occurrence of each element in array
    for(int i=0;i<len;i++){

        countArr[arr[i]]++;
    }

     // 4. Find cumulative sum of countArr
    
    for(int i=1;i<=max;i++){

        countArr[i] = countArr[i-1]+countArr[i];
    }

    //5.Create new array of size len

    int output[len];

    for(int i = len-1;i>=0;i--){

        output[countArr[arr[i]]-1] = arr[i];
        countArr[arr[i]]--;
    }

    //6.Copy element to original array

    for(int i=0;i<len;i++){

        arr[i] = output[i];
    }
}


void countingSort_Negative(int arr[],int len){

    int min = arr[0],max = arr[0];


    //1. Find min and max of array
    for(int i=1;i<len;i++){

        if(max<arr[i])
            max = arr[i];

        if(min>arr[i])
            min = arr[i];
    }

   
   // 2. Subtract min term for each element of array and max element

    for(int i=0;i<len;i++){

        arr[i] = arr[i]-min;
    }


    max = max-min;


    // 4. Define array of size max+1 and assign all elements to 0;
    int countArr[max+1];

    for(int i=0;i<=max;i++){

        countArr[i] = 0;
    }


     // 5. Count the occurrence of each element in array
    for(int i=0;i<len;i++){

        countArr[arr[i]]++;
        
    }


    // 6. Find cumulative sum of countArr
    
     for(int i=1;i<=max;i++){

        countArr[i] = countArr[i-1]+countArr[i];
    }


    //7.Create new array of size len

    int output[len];

    for(int i = len-1;i>=0;i--){

        output[countArr[arr[i]]-1] = arr[i];
        countArr[arr[i]]--;
    }

    //8.Copy element to original array and add min term to get original array

    for(int i=0;i<len;i++){

        arr[i] = output[i]+min;
    }


}


void main(){

    int arr[] = {3,7,2,1,8,2,5,2,7};

    int len = sizeof(arr)/sizeof(int);

    printf("Before Sorting: ");

    for(int i=0;i<len;i++){

        printf("%d ",arr[i]);
    }

    countingSort_NonNegative(arr,len);

    printf("\nAfter Sorting ");

    for(int i=0;i<len;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");

    int arr1[] = {-3,7,-2,1,8,2,5,2,7,-4};

    int len1 = sizeof(arr1)/sizeof(int);

    printf("Before Sorting: ");

    for(int i=0;i<len1;i++){

        printf("%d ",arr1[i]);
    }

    countingSort_Negative(arr1,len1);

   printf("\nAfter Sorting ");

    for(int i=0;i<len1;i++){

        printf("%d ",arr1[i]);
    }

    printf("\n");

}