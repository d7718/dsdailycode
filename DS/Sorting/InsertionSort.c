#include<stdio.h>

void insertionSort(int arr[],int len){

    for(int i=1;i<len;i++){

        int target = arr[i],j=i-1;

        for(;j>=0 && target<arr[j];j--){

            arr[j+1] = arr[j];
        }

        arr[j+1] =  target;
        printf("%d\n",arr[j+1]);
    }
}

void main(){

    int arr[] = {2,-7,-5,3,1,8,5};

    insertionSort(arr,7);

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");
    
}