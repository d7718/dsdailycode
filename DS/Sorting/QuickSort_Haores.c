#include<stdio.h>


void swap(int *a,int*b){

    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int arr[],int start,int end){

    int pivot = arr[start];
    int i = start-1;
    int j = end+1;

    while(1){

        do{
            i++;

        }while(arr[i]<pivot);

        do{

            j--;

        }while(arr[j]>pivot);

        if(i>=j)
            return j;
        
        swap(&arr[i],&arr[j]);
    }
}

void quickSort(int arr[],int start,int end){

    if(start<end){

        int pivot = partition(arr,start,end);
        quickSort(arr,start,pivot);
        quickSort(arr,pivot+1,end);
    }
}


void main(){

    int arr[] = {3,4,2,7,33,45,9};

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    int start=0,end=6;
    quickSort(arr,start,end);

    printf("\nAfter QuickSort\n");

    for(int i=0;i<7;i++){

        printf("%d ",arr[i]);
    }

    printf("\n");
}